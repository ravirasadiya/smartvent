-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 16, 2020 at 11:35 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smartevnt`
--

-- --------------------------------------------------------

--
-- Table structure for table `additional_infos`
--

CREATE TABLE `additional_infos` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tehsil_id` int(11) DEFAULT NULL,
  `village_id` int(11) DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `crop_category` int(11) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attribute_values`
--

CREATE TABLE `attribute_values` (
  `id` int(10) UNSIGNED NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `attribute_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attribute_value_product_attribute`
--

CREATE TABLE `attribute_value_product_attribute` (
  `attribute_value_id` int(10) UNSIGNED NOT NULL,
  `product_attribute_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `shipping_price` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_price` decimal(8,2) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `product_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gst` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gst_price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `offer_id` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_hi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_gu` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_hi` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_gu` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `cover` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `_lft` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `_rgt` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `company_id` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `category_product`
--

CREATE TABLE `category_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_content`
--

CREATE TABLE `cms_content` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `div_id` int(11) NOT NULL,
  `html` text DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cms_content`
--

INSERT INTO `cms_content` (`id`, `page_id`, `div_id`, `html`, `image`, `title`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 3, '<', '/storage/services/founder-image.png', 'asfdsaf', 1, '2020-10-16 01:41:23', '2020-10-16 01:41:23'),
(2, 1, 3, 'p', '/storage/services/company-images.png', 'sfdasdfasfd', 1, '2020-10-16 01:41:23', '2020-10-16 01:41:23'),
(3, 1, 3, '>', '/storage/services/founder-image.png', 'sfasdfasdf', 1, '2020-10-16 01:41:23', '2020-10-16 01:41:23'),
(4, 1, 3, 'a', '/storage/services/founder-image.png', 'sdfasdfasdf', 1, '2020-10-16 01:41:23', '2020-10-16 01:41:23'),
(5, 1, 3, 'sadfasdf', '/storage/services/founder-image.png', 'sdfasdf', 1, '2020-10-16 01:57:48', '2020-10-16 01:57:48'),
(6, 1, 3, '<p>sadfasdf</p>', '/storage/services/company-images.png', 'sdfasdf', 1, '2020-10-16 01:58:44', '2020-10-16 01:58:44'),
(7, 1, 3, 'asdfasdf', '/storage/services/founder-image.png', 'asdfasdf', 1, '2020-10-16 01:58:44', '2020-10-16 01:58:44');

-- --------------------------------------------------------

--
-- Table structure for table `couriers`
--

CREATE TABLE `couriers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_free` int(11) NOT NULL,
  `cost` decimal(8,2) DEFAULT 0.00,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `crop`
--

CREATE TABLE `crop` (
  `id` int(11) NOT NULL,
  `crop_category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `crop_sub` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_hi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_gu` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `product_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `stripe_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_brand` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `tehsil_id` int(11) DEFAULT NULL,
  `village_id` int(11) DEFAULT NULL,
  `language` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `otp` int(11) DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customer_notifications`
--

CREATE TABLE `customer_notifications` (
  `id` int(10) NOT NULL,
  `company_id` int(10) NOT NULL,
  `customer_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `msg` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'NULL',
  `state_id` int(11) DEFAULT 0,
  `city_id` int(11) DEFAULT 0,
  `tehsil_id` int(11) DEFAULT 0,
  `status` int(10) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_hi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_gu` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `feature`
--

CREATE TABLE `feature` (
  `id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `hidding` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `home_silder`
--

CREATE TABLE `home_silder` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `page` varchar(255) NOT NULL,
  `plant_content` text NOT NULL,
  `status` int(11) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `home_silder`
--

INSERT INTO `home_silder` (`id`, `image`, `link`, `title`, `page`, `plant_content`, `status`, `created_at`, `updated_at`) VALUES
(1, '/storage/silders/template.jpg', 'http://127.0.0.1:8000/adm', 'Slider', 'home', '<p>Good</p>', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE `module` (
  `id` int(10) NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`id`, `display_name`, `name`) VALUES
(1, 'Products', 'product'),
(2, 'Crop', 'crop'),
(3, 'Technical Category', 'technical'),
(4, 'Categories', 'category'),
(5, 'Customers', 'customer'),
(6, 'Update Stock', 'update_stock'),
(7, 'Offers', 'offer'),
(8, 'Orders', 'order'),
(9, 'Order Statuses', 'order_status'),
(10, 'Companies', 'company'),
(11, 'Role', 'role'),
(12, 'Priority', 'priority'),
(13, 'Notification', 'notification'),
(14, 'Country Data', 'country_data'),
(15, 'Wallet', 'wallet'),
(16, 'Users', 'users');

-- --------------------------------------------------------

--
-- Table structure for table `module_permission`
--

CREATE TABLE `module_permission` (
  `id` int(10) NOT NULL,
  `module_id` int(10) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `view` tinyint(1) DEFAULT 0,
  `add` tinyint(1) DEFAULT 0,
  `edit` tinyint(1) DEFAULT 0,
  `delete` tinyint(1) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `module_permission`
--

INSERT INTO `module_permission` (`id`, `module_id`, `user_id`, `view`, `add`, `edit`, `delete`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 1, '2019-11-26 20:00:25', '2020-03-23 21:42:12'),
(2, 2, 1, 1, 1, 1, 1, '2019-11-26 20:00:25', '2020-03-23 21:42:12'),
(3, 3, 1, 1, 1, 1, 1, '2019-11-26 20:00:25', '2020-03-23 21:42:12'),
(4, 4, 1, 1, 1, 1, 1, '2019-11-26 20:00:25', '2020-03-23 21:42:12'),
(5, 5, 1, 1, 1, 1, 1, '2019-11-26 20:00:25', '2020-03-23 21:42:12'),
(6, 6, 1, 1, 1, 1, 1, '2019-11-26 20:00:25', '2020-03-23 21:42:12'),
(7, 7, 1, 1, 1, 1, 1, '2019-11-26 20:00:25', '2020-03-23 21:42:12'),
(8, 8, 1, 1, 1, 1, 1, '2019-11-26 20:00:25', '2020-03-23 21:42:12'),
(9, 9, 1, 1, 1, 1, 1, '2019-11-26 20:00:25', '2020-03-23 21:42:12'),
(10, 10, 1, 1, 1, 1, 1, '2019-11-26 20:00:25', '2020-03-23 21:42:12'),
(11, 11, 1, 1, 1, 1, 1, '2019-11-26 20:00:25', '2020-03-23 21:42:12'),
(12, 12, 1, 1, 0, 1, 0, '2019-11-26 20:00:56', '2020-03-23 21:42:12'),
(13, 13, 1, 1, 1, 1, 1, '2019-11-26 20:00:56', '2020-03-23 21:42:12'),
(14, 3, 2, 1, 1, 1, 1, '2019-11-26 20:00:56', '2020-03-24 19:01:37'),
(15, 4, 2, 1, 1, 1, 1, '2019-11-26 20:00:56', '2020-03-24 19:01:37'),
(16, 5, 2, 1, 1, 1, 1, '2019-11-26 20:00:56', '2020-03-24 19:01:37'),
(17, 6, 2, 1, 0, 1, 0, '2019-11-26 20:00:56', '2020-03-24 19:01:37'),
(18, 7, 2, 1, 1, 1, 1, '2019-11-26 20:00:56', '2020-03-24 19:01:37'),
(19, 8, 2, 1, 1, 1, 1, '2019-11-26 20:00:56', '2020-03-24 19:01:37'),
(20, 9, 2, 1, 0, 1, 0, '2019-11-26 20:00:56', '2020-03-24 19:01:37'),
(21, 10, 2, 1, 1, 1, 1, '2019-11-26 20:00:56', '2020-03-24 19:01:37'),
(22, 11, 2, 1, 1, 1, 1, '2019-11-26 20:00:56', '2020-03-24 19:01:37'),
(23, 1, 3, 1, 1, 1, 1, '2019-11-26 20:03:35', '2019-12-28 12:14:12'),
(24, 2, 3, 1, 1, 1, 1, '2019-11-26 20:03:35', '2019-12-28 12:14:12'),
(25, 3, 3, 1, 1, 1, 0, '2019-11-26 20:03:35', '2019-12-28 12:14:12'),
(26, 4, 3, 1, 1, 1, 1, '2019-11-26 20:03:35', '2019-12-28 12:14:12'),
(27, 5, 3, 1, 1, 1, 1, '2019-11-26 20:03:35', '2019-12-28 12:14:12'),
(28, 6, 3, 1, 1, 1, 1, '2019-11-26 20:03:35', '2019-12-28 12:14:12'),
(29, 7, 3, 1, 1, 1, 1, '2019-11-26 20:03:35', '2019-12-28 12:14:12'),
(30, 8, 3, 1, 1, 1, 1, '2019-11-26 20:03:35', '2019-12-28 12:14:12'),
(31, 9, 3, 1, 1, 1, 1, '2019-11-26 20:03:35', '2019-12-28 12:14:12'),
(32, 10, 3, 1, 1, 1, 1, '2019-11-26 20:03:35', '2019-12-28 12:14:12'),
(33, 11, 3, 1, 1, 1, 1, '2019-11-26 20:03:35', '2019-12-28 12:14:12'),
(34, 12, 3, 1, 1, 1, 1, '2019-12-13 07:09:58', '2019-12-28 12:14:12'),
(35, 13, 3, 1, 1, 1, 1, '2019-12-13 07:10:17', '2019-12-28 12:14:12'),
(36, 1, 2, 1, 1, 1, 1, '2019-12-13 07:10:37', '2020-03-24 19:01:37'),
(37, 13, 2, 0, 1, 0, 0, '2019-12-13 07:10:56', '2020-03-24 19:01:37'),
(38, 14, 1, 1, 1, 1, 1, '2020-02-04 05:59:54', '2020-03-23 21:42:12'),
(39, 14, 2, 1, 1, 1, 1, '2020-02-04 06:00:21', '2020-03-24 19:01:37'),
(40, 14, 3, 1, 1, 1, 1, '2020-02-04 06:00:42', '2020-02-04 06:00:42'),
(60, 2, 2, 1, 1, 1, 1, '2019-12-13 12:08:54', '2020-03-24 19:01:37'),
(105, 12, 2, 1, 0, 1, 0, '2019-12-14 08:51:01', '2020-03-24 19:01:37'),
(106, 1, 4, 1, 0, 0, 0, '2020-03-17 22:04:45', '2020-03-21 16:45:48'),
(107, 2, 4, 1, 0, 0, 0, '2020-03-17 22:04:45', '2020-03-21 16:45:48'),
(108, 3, 4, 1, 0, 0, 0, '2020-03-17 22:04:45', '2020-03-21 16:45:48'),
(109, 4, 4, 1, 0, 0, 0, '2020-03-17 22:04:45', '2020-03-21 16:45:48'),
(110, 5, 4, 1, 0, 0, 0, '2020-03-17 22:04:45', '2020-03-21 16:45:48'),
(111, 6, 4, 1, 0, 0, 0, '2020-03-17 22:04:45', '2020-03-21 16:45:48'),
(112, 7, 4, 1, 0, 0, 0, '2020-03-17 22:04:45', '2020-03-21 16:45:48'),
(113, 8, 4, 1, 0, 0, 0, '2020-03-17 22:04:45', '2020-03-21 16:45:48'),
(114, 9, 4, 1, 0, 0, 0, '2020-03-17 22:04:45', '2020-03-21 16:45:48'),
(115, 10, 4, 1, 0, 0, 0, '2020-03-17 22:04:45', '2020-03-21 16:45:48'),
(116, 11, 4, 1, 0, 0, 0, '2020-03-17 22:04:45', '2020-03-21 16:45:48'),
(117, 12, 4, 1, 0, 0, 0, '2020-03-17 22:04:45', '2020-03-21 16:45:48'),
(118, 13, 4, 0, 0, 0, 0, '2020-03-17 22:04:45', '2020-03-21 16:45:30'),
(119, 14, 4, 1, 0, 0, 0, '2020-03-17 22:04:45', '2020-03-21 16:45:48'),
(120, 15, 1, 1, 0, 1, 0, '2020-03-23 10:31:13', '2020-03-23 21:42:12'),
(121, 15, 2, 1, 0, 0, 0, '2020-03-23 10:31:36', '2020-03-24 19:01:37'),
(122, 15, 3, 0, 0, 0, 0, '2020-03-23 10:31:59', '2020-03-23 10:31:59'),
(123, 15, 4, 0, 0, 0, 0, '2020-03-23 10:32:16', '2020-03-23 10:32:16'),
(124, 1, 5, 1, 1, 1, 1, '2020-06-22 18:12:02', '2020-06-22 18:12:02'),
(125, 2, 5, 1, 1, 1, 1, '2020-06-22 18:12:02', '2020-06-22 18:12:02'),
(126, 3, 5, 1, 1, 1, 1, '2020-06-22 18:12:02', '2020-06-22 18:12:02'),
(127, 4, 5, 1, 1, 1, 1, '2020-06-22 18:12:02', '2020-06-22 18:12:02'),
(128, 5, 5, 1, 1, 1, 1, '2020-06-22 18:12:02', '2020-06-22 18:12:02'),
(129, 6, 5, 1, 0, 1, 0, '2020-06-22 18:12:02', '2020-06-22 18:12:02'),
(130, 7, 5, 1, 1, 1, 1, '2020-06-22 18:12:02', '2020-06-22 18:12:02'),
(131, 8, 5, 1, 1, 1, 1, '2020-06-22 18:12:02', '2020-06-22 18:12:02'),
(132, 9, 5, 1, 0, 1, 0, '2020-06-22 18:12:02', '2020-06-22 18:12:02'),
(133, 10, 5, 1, 1, 1, 1, '2020-06-22 18:12:02', '2020-06-22 18:12:02'),
(134, 11, 5, 1, 1, 1, 1, '2020-06-22 18:12:02', '2020-06-22 18:12:02'),
(135, 12, 5, 1, 0, 1, 0, '2020-06-22 18:12:02', '2020-06-22 18:12:02'),
(136, 13, 5, 0, 1, 0, 0, '2020-06-22 18:12:02', '2020-06-22 18:12:02'),
(137, 14, 5, 1, 1, 1, 1, '2020-06-22 18:12:02', '2020-06-22 18:12:02'),
(138, 15, 5, 1, 0, 0, 0, '2020-06-22 18:12:02', '2020-06-22 18:12:02'),
(139, 16, 5, 1, 1, 1, 1, '2020-06-22 18:12:02', '2020-06-22 18:12:02'),
(140, 16, 2, 1, 1, 1, 1, '2020-06-22 12:12:07', '2020-06-22 12:12:07'),
(141, 16, 1, 1, 1, 1, 1, '2020-06-22 12:13:11', '2020-06-22 12:13:11'),
(142, 1, 6, 0, 0, 0, 0, '2020-09-16 17:18:59', '2020-09-16 17:18:59'),
(143, 2, 6, 0, 0, 0, 0, '2020-09-16 17:18:59', '2020-09-16 17:18:59'),
(144, 3, 6, 0, 0, 0, 0, '2020-09-16 17:18:59', '2020-09-16 17:18:59'),
(145, 4, 6, 0, 0, 0, 0, '2020-09-16 17:18:59', '2020-09-16 17:18:59'),
(146, 5, 6, 0, 0, 0, 0, '2020-09-16 17:18:59', '2020-09-16 17:18:59'),
(147, 6, 6, 0, 0, 0, 0, '2020-09-16 17:18:59', '2020-09-16 17:18:59'),
(148, 7, 6, 0, 0, 0, 0, '2020-09-16 17:18:59', '2020-09-16 17:18:59'),
(149, 8, 6, 0, 0, 0, 0, '2020-09-16 17:18:59', '2020-09-16 17:18:59'),
(150, 9, 6, 0, 0, 0, 0, '2020-09-16 17:18:59', '2020-09-16 17:18:59'),
(151, 10, 6, 0, 0, 0, 0, '2020-09-16 17:18:59', '2020-09-16 17:18:59'),
(152, 11, 6, 0, 0, 0, 0, '2020-09-16 17:18:59', '2020-09-16 17:18:59'),
(153, 12, 6, 0, 0, 0, 0, '2020-09-16 17:18:59', '2020-09-16 17:18:59'),
(154, 13, 6, 0, 0, 0, 0, '2020-09-16 17:18:59', '2020-09-16 17:18:59'),
(155, 14, 6, 0, 0, 0, 0, '2020-09-16 17:18:59', '2020-09-16 17:18:59'),
(156, 15, 6, 0, 0, 0, 0, '2020-09-16 17:18:59', '2020-09-16 17:18:59'),
(157, 16, 6, 0, 0, 0, 0, '2020-09-16 17:18:59', '2020-09-16 17:18:59'),
(158, 1, 7, 1, 0, 0, 0, '2020-09-16 17:19:52', '2020-09-16 17:19:52'),
(159, 2, 7, 1, 0, 0, 0, '2020-09-16 17:19:52', '2020-09-16 17:19:52'),
(160, 3, 7, 1, 0, 0, 0, '2020-09-16 17:19:52', '2020-09-16 17:19:52'),
(161, 4, 7, 1, 0, 0, 0, '2020-09-16 17:19:52', '2020-09-16 17:19:52'),
(162, 5, 7, 1, 0, 0, 0, '2020-09-16 17:19:52', '2020-09-16 17:19:52'),
(163, 6, 7, 0, 0, 0, 0, '2020-09-16 17:19:52', '2020-09-16 17:19:52'),
(164, 7, 7, 0, 0, 1, 0, '2020-09-16 17:19:52', '2020-09-16 17:19:52'),
(165, 8, 7, 0, 0, 0, 0, '2020-09-16 17:19:52', '2020-09-16 17:19:52'),
(166, 9, 7, 0, 0, 0, 0, '2020-09-16 17:19:52', '2020-09-16 17:19:52'),
(167, 10, 7, 0, 0, 0, 0, '2020-09-16 17:19:52', '2020-09-16 17:19:52'),
(168, 11, 7, 0, 0, 0, 0, '2020-09-16 17:19:52', '2020-09-16 17:19:52'),
(169, 12, 7, 0, 0, 0, 0, '2020-09-16 17:19:52', '2020-09-16 17:19:52'),
(170, 13, 7, 0, 0, 0, 0, '2020-09-16 17:19:52', '2020-09-16 17:19:52'),
(171, 14, 7, 0, 0, 0, 0, '2020-09-16 17:19:52', '2020-09-16 17:19:52'),
(172, 15, 7, 0, 0, 0, 0, '2020-09-16 17:19:52', '2020-09-16 17:19:52'),
(173, 16, 7, 0, 0, 0, 0, '2020-09-16 17:19:52', '2020-09-16 17:19:52');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `notify_detail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notification_response`
--

CREATE TABLE `notification_response` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `response` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(11) NOT NULL,
  `offer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `offer_hi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `offer_gu` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `cover` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `offer_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expired_date` date DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `parent_id` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `expired_time` time DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_hi` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_gu` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `offer_product`
--

CREATE TABLE `offer_product` (
  `id` int(11) NOT NULL,
  `offer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `reference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `courier_id` int(10) UNSIGNED NOT NULL,
  `courier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mail_status` int(10) DEFAULT 0,
  `address_id` int(10) UNSIGNED NOT NULL,
  `order_status_id` int(10) UNSIGNED NOT NULL COMMENT '1=delivered, 2=transit, 3=order 4=cancel',
  `payment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `discounts` decimal(8,2) NOT NULL DEFAULT 0.00,
  `total_products` decimal(8,2) NOT NULL,
  `total_shipping` decimal(8,2) NOT NULL DEFAULT 0.00,
  `tax` decimal(8,2) NOT NULL DEFAULT 0.00,
  `total` decimal(8,2) NOT NULL,
  `total_paid` decimal(8,2) NOT NULL DEFAULT 0.00,
  `invoice` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_path` varchar(155) COLLATE utf8_unicode_ci DEFAULT NULL,
  `label_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tracking_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wallet_point` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_print`
--

CREATE TABLE `order_print` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE `order_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `attribute_id` int(10) UNSIGNED DEFAULT NULL,
  `offer_id` int(11) DEFAULT 0,
  `quantity` int(11) NOT NULL,
  `product_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_sku` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_price` decimal(8,2) DEFAULT NULL,
  `shipping_price` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_statuses`
--

CREATE TABLE `order_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(10) NOT NULL,
  `device` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `transaction_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `userid` int(10) NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'create-product', 'Create product', '', '2019-10-03 06:07:22', '2019-10-03 06:07:22'),
(2, 'view-product', 'View product', '', '2019-10-03 06:07:22', '2019-10-03 06:07:22'),
(3, 'update-product', 'Update product', '', '2019-10-03 06:07:22', '2019-10-03 06:07:22'),
(4, 'delete-product', 'Delete product', '', '2019-10-03 06:07:22', '2019-10-03 06:07:22'),
(5, 'update-order', 'Update order', '', '2019-10-03 06:07:22', '2019-10-03 06:07:22');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(2, 3),
(3, 1),
(3, 2),
(3, 3),
(4, 1),
(4, 2),
(5, 1),
(5, 2);

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `priorities`
--

CREATE TABLE `priorities` (
  `id` int(11) NOT NULL,
  `priority` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(10) DEFAULT NULL,
  `subcategory_id` int(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `priorities`
--

INSERT INTO `priorities` (`id`, `priority`, `name`, `category_id`, `subcategory_id`, `created_at`, `updated_at`) VALUES
(1, 2, 'Product Categories / Manage Products', NULL, NULL, '2019-11-25 12:35:43', '2020-09-29 17:53:25'),
(2, 3, 'Company List', NULL, NULL, '2019-11-25 12:35:43', '2020-09-17 12:35:40'),
(3, 1, 'Trending Products', NULL, NULL, '2019-11-25 12:35:43', '2020-09-29 17:53:25');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `brand_id` int(10) UNSIGNED DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `crop_id` int(11) DEFAULT NULL,
  `crop_category` int(11) DEFAULT NULL,
  `subcropcategory_id` int(11) DEFAULT NULL,
  `sku` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_hi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_gu` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_hi` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_gu` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `cover` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_price` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `quantity` int(11) DEFAULT 0,
  `hsn_code` int(11) DEFAULT NULL,
  `price` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `sale_price` varchar(10) COLLATE utf8_unicode_ci DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT 0,
  `length` decimal(8,2) DEFAULT NULL,
  `width` decimal(8,2) DEFAULT NULL,
  `height` decimal(8,2) DEFAULT NULL,
  `distance_unit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` varchar(10) COLLATE utf8_unicode_ci DEFAULT '0.00',
  `mass_unit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `unit` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `technical_name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `technical_id` int(11) DEFAULT NULL,
  `available_quantity` int(11) DEFAULT 0,
  `stock_status` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mail_status` int(10) DEFAULT 0,
  `additional_info` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `additional_info_hi` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `additional_info_gu` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `offer_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `offer_title_hi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `offer_title_gu` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `offer_product_hi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `offer_product_gu` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `offer_product` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `offer_quantity` int(11) DEFAULT 0,
  `offer_product_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_attribute`
--

CREATE TABLE `product_attribute` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(35) COLLATE utf8_unicode_ci DEFAULT '0',
  `price` varchar(35) COLLATE utf8_unicode_ci DEFAULT '0.00',
  `sale_price` varchar(35) COLLATE utf8_unicode_ci DEFAULT '0',
  `quantity` int(35) UNSIGNED DEFAULT 0,
  `available_quantity` int(35) UNSIGNED DEFAULT 0,
  `discount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gst_price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gst_less_price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_attributes`
--

CREATE TABLE `product_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(8,2) DEFAULT NULL,
  `sale_price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `default` tinyint(4) NOT NULL DEFAULT 0,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `src` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

CREATE TABLE `provinces` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'Super Admin', '', '2019-10-03 06:07:23', '2019-10-03 06:07:23'),
(2, 'admin', 'Admin', '', '2019-10-03 06:07:23', '2019-10-03 06:07:23'),
(3, 'clerk', 'Clerk', '', '2019-10-03 06:07:24', '2019-10-03 06:07:24'),
(4, 'PQR', 'ABC', 'This is from Role.', '2020-03-17 22:04:45', '2020-03-21 16:45:30'),
(5, 'Test', '123', '<p>123</p>', '2020-06-22 18:12:02', '2020-06-22 18:12:02'),
(6, 'Econs', 'chintan', NULL, '2020-09-16 17:18:59', '2020-09-16 17:18:59'),
(7, 'Rajkot', 'chintan', NULL, '2020-09-16 17:19:52', '2020-09-16 17:19:52');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(11) NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `user_type`) VALUES
(1, 1, 1, 'App\\Shop\\Employees\\Employee'),
(2, 2, 2, 'App\\Shop\\Employees\\Employee'),
(3, 3, 3, 'App\\Shop\\Employees\\Employee'),
(4, 2, 4, 'App\\Shop\\Employees\\Employee'),
(5, 1, 5, 'App\\Shop\\Employees\\Employee'),
(6, 3, 6, 'App\\Shop\\Employees\\Employee'),
(7, 3, 65, 'App\\Shop\\Employees\\Employee'),
(8, 3, 66, 'App\\Shop\\Employees\\Employee'),
(9, 2, 67, 'App\\Shop\\Employees\\Employee'),
(11, 3, 69, 'App\\Shop\\Employees\\Employee'),
(12, 2, 70, 'App\\Shop\\Employees\\Employee'),
(13, 9, 15, 'App\\Shop\\Employees\\Employee'),
(14, 1, 16, 'App\\Shop\\Employees\\Employee'),
(15, 1, 18, 'App\\Shop\\Employees\\Employee'),
(16, 3, 21, 'App\\Shop\\Employees\\Employee'),
(17, 1, 22, 'App\\Shop\\Employees\\Employee'),
(18, 1, 23, 'App\\Shop\\Employees\\Employee'),
(19, 2, 24, 'App\\Shop\\Employees\\Employee'),
(20, 2, 26, 'App\\Shop\\Employees\\Employee'),
(21, 9, 27, 'App\\Shop\\Employees\\Employee'),
(22, 2, 30, 'App\\Shop\\Employees\\Employee'),
(23, 2, 29, 'App\\Shop\\Employees\\Employee'),
(24, 10, 31, 'App\\Shop\\Employees\\Employee'),
(25, 2, 33, 'App\\Shop\\Employees\\Employee'),
(26, 2, 32, 'App\\Shop\\Employees\\Employee'),
(27, 2, 28, 'App\\Shop\\Employees\\Employee'),
(28, 2, 25, 'App\\Shop\\Employees\\Employee'),
(29, 2, 20, 'App\\Shop\\Employees\\Employee'),
(30, 2, 19, 'App\\Shop\\Employees\\Employee'),
(31, 2, 17, 'App\\Shop\\Employees\\Employee'),
(32, 2, 34, 'App\\Shop\\Employees\\Employee'),
(33, 1, 36, 'App\\Shop\\Employees\\Employee'),
(34, 2, 35, 'App\\Shop\\Employees\\Employee'),
(35, 2, 37, 'App\\Shop\\Employees\\Employee'),
(36, 1, 38, 'App\\Shop\\Employees\\Employee'),
(37, 2, 39, 'App\\Shop\\Employees\\Employee'),
(38, 1, 40, 'App\\Shop\\Employees\\Employee'),
(39, 2, 41, 'App\\Shop\\Employees\\Employee'),
(40, 2, 42, 'App\\Shop\\Employees\\Employee'),
(44, 5, 48, 'App\\Shop\\Employees\\Employee'),
(45, 6, 2, 'App\\Shop\\Employees\\Employee');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `shoppingcart`
--

CREATE TABLE `shoppingcart` (
  `identifier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instance` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `smart_vent`
--

CREATE TABLE `smart_vent` (
  `id` int(11) NOT NULL,
  `hidding` varchar(255) DEFAULT NULL,
  `dscription` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(11) NOT NULL,
  `state` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `state`, `country_id`) VALUES
(1, 'ANDHRA PRADESH', 105),
(2, 'ASSAM', 105),
(3, 'ARUNACHAL PRADESH', 105),
(4, 'BIHAR', 105),
(5, 'GUJRAT', 105),
(6, 'HARYANA', 105),
(7, 'HIMACHAL PRADESH', 105),
(8, 'JAMMU & KASHMIR', 105),
(9, 'KARNATAKA', 105),
(10, 'KERALA', 105),
(11, 'MADHYA PRADESH', 105),
(12, 'MAHARASHTRA', 105),
(13, 'MANIPUR', 105),
(14, 'MEGHALAYA', 105),
(15, 'MIZORAM', 105),
(16, 'NAGALAND', 105),
(17, 'ORISSA', 105),
(18, 'PUNJAB', 105),
(19, 'RAJASTHAN', 105),
(20, 'SIKKIM', 105),
(21, 'TAMIL NADU', 105),
(22, 'TRIPURA', 105),
(23, 'UTTAR PRADESH', 105),
(24, 'WEST BENGAL', 105),
(25, 'DELHI', 105),
(26, 'GOA', 105),
(27, 'PONDICHERY', 105),
(28, 'LAKSHDWEEP', 105),
(29, 'DAMAN & DIU', 105),
(30, 'DADRA & NAGAR', 105),
(31, 'CHANDIGARH', 105),
(32, 'ANDAMAN & NICOBAR', 105),
(33, 'UTTARANCHAL', 105),
(34, 'JHARKHAND', 105),
(35, 'CHATTISGARH', 105);

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stripe_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stripe_plan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `ends_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `technicals`
--

CREATE TABLE `technicals` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_hi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_gu` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `trending_category`
--

CREATE TABLE `trending_category` (
  `id` int(10) NOT NULL,
  `priority` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_hi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_gu` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `name_hi`, `name_gu`, `email`, `password`, `logo`, `status`, `deleted_at`, `remember_token`, `created_at`, `updated_at`, `mobile`) VALUES
(1, 'smartvent', NULL, NULL, 'admin@khedutbolo.com', '$2y$10$pM53moH6e5Yx5vHk/pprbuSBOHnLBaTEpHWriXWbV9/5ubl4Ng76C', 'http://khedutbolo.com/storage/logo/142e146bbe6a644608a57749c29ae707.jpg', 1, NULL, NULL, '2020-03-11 17:17:25', '2020-06-18 11:10:32', NULL),
(2, 'chintan', 'चिंतन', 'ચિંતા', 'chintanpan05@gmail.com', '$2y$10$SDzSDb/CAh9ufYWFtmNZIeSaTDkDto7rNLLnhbKgR3l/6GG6zmD8y', 'http://khedutbolo.com/storage/logo/989885f449f92acbdc26b7de9ab29eee.jpg', 1, NULL, NULL, NULL, NULL, '7567707501');

-- --------------------------------------------------------

--
-- Table structure for table `wallet`
--

CREATE TABLE `wallet` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `points` int(11) DEFAULT NULL,
  `credit_or_debit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `additional_infos`
--
ALTER TABLE `additional_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_content`
--
ALTER TABLE `cms_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crop`
--
ALTER TABLE `crop`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_notifications`
--
ALTER TABLE `customer_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feature`
--
ALTER TABLE `feature`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_silder`
--
ALTER TABLE `home_silder`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module_permission`
--
ALTER TABLE `module_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_response`
--
ALTER TABLE `notification_response`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer_product`
--
ALTER TABLE `offer_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_print`
--
ALTER TABLE `order_print`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_statuses`
--
ALTER TABLE `order_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `priorities`
--
ALTER TABLE `priorities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_attribute`
--
ALTER TABLE `product_attribute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smart_vent`
--
ALTER TABLE `smart_vent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `technicals`
--
ALTER TABLE `technicals`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `trending_category`
--
ALTER TABLE `trending_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `priority` (`priority`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wallet`
--
ALTER TABLE `wallet`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `additional_infos`
--
ALTER TABLE `additional_infos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_content`
--
ALTER TABLE `cms_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `crop`
--
ALTER TABLE `crop`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer_notifications`
--
ALTER TABLE `customer_notifications`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feature`
--
ALTER TABLE `feature`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `home_silder`
--
ALTER TABLE `home_silder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `module`
--
ALTER TABLE `module`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `module_permission`
--
ALTER TABLE `module_permission`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=174;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notification_response`
--
ALTER TABLE `notification_response`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `offer_product`
--
ALTER TABLE `offer_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_print`
--
ALTER TABLE `order_print`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1013;

--
-- AUTO_INCREMENT for table `order_product`
--
ALTER TABLE `order_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_statuses`
--
ALTER TABLE `order_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `priorities`
--
ALTER TABLE `priorities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_attribute`
--
ALTER TABLE `product_attribute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `smart_vent`
--
ALTER TABLE `smart_vent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `technicals`
--
ALTER TABLE `technicals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trending_category`
--
ALTER TABLE `trending_category`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wallet`
--
ALTER TABLE `wallet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
