@extends('layouts.front.app')

@section('content')
<!-- Banner Block -->
<section class="banner_wrap">
    <div class="container bnr_content">
        <div class="row">
            <div class="col-md-6 offset-md-6">
                <div class="banner_content">
                   @if(isset($silders))
                      <h1>{!! $silders['title'] !!}</h1>
                       <a href="{{$silders['link']}}" class="btn_1" target="_blank">Find a Dealer</a>
                     @endif
                </div>
            </div>
        </div>
    </div>
    <div class="banner_info_strip">
        <div class="container">
            <ol class="banner_pts">
                <li class="breadcrumb-item"><a>Crawlspaces</a></li>
                <li class="breadcrumb-item"><a>Garages</a></li>
                <li class="breadcrumb-item"><a>Enclosures</a></li>
                <li class="breadcrumb-item"><a>Elevated on Pilings</a></li>
                <li class="breadcrumb-item"><a>Detached Buildings</a></li>
                <li class="breadcrumb-item"><a>Walkout Enclosures</a></li>
                <li class="breadcrumb-item"><a>Commercial Buildings</a></li>
            </ol>
        </div>
    </div>
</section>
<!-- End -->

<!-- Plantation Block -->
<section class="plantation_wrapper">
<div class="container">
    <div class="row">
        <div class="col-md-7">
            <div class="plant_content">
            <h2>@if(isset($cmscontentdiv1)) {!! $cmscontentdiv1['html'] !!} @endif</h2>
            </div>
        </div>
        <div class="col-md-5">
            <div class="plant_img">
                <img src="{{ asset('front_assets/images/feature_image.png') }}" alt="Image" class="img-fluid">
            </div>
        </div>
    </div>
</div>
</section>
<!-- End -->

<!-- Popular Modals Block -->
<section class="popular_modals_wrap">
    <div class="container">
        <div class="header">
            <h2 class="h_txt">Popular <span>Models</span></h2>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="model_block">
                    <img src="{{ asset('front_assets/images/product_img.jpg') }}" alt="Image" class="img-fluid">
                    <div class="model_inner">
                        <h3>Product Title</h3>
                        <p>Product Description Goes Here</p>
                        <a href="#" class="btn_2">Find Dealer</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="model_block">
                    <img src="{{ asset('front_assets/images/product_img.jpg') }}" alt="Image" class="img-fluid">
                     <div class="model_inner">
                        <h3>Product Title</h3>
                        <p>Product Description Goes Here</p>
                        <a href="#" class="btn_2">Find Dealer</a>
                    </div>
                 </div>
            </div>
            <div class="col-md-3">
                <div class="model_block">
                    <img src="{{ asset('front_assets/images/product_img.jpg') }}" alt="Image" class="img-fluid">
                    <div class="model_inner">
                        <h3>Product Title</h3>
                        <p>Product Description Goes Here</p>
                        <a href="#" class="btn_2">Find Dealer</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="model_block">
                    <img src="{{ asset('front_assets/images/product_img.jpg') }}" alt="Image" class="img-fluid">
                    <div class="model_inner">
                        <h3>Product Title</h3>
                        <p>Product Description Goes Here</p>
                        <a href="#" class="btn_2">Find Dealer</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->

<!-- Why SmartVent Block -->
<section class="whyus_wrapper">
<div class="whyus_header">
    <div class="whyus_header_inner">
        <h2>@if(isset($cmscontentdiv2)) {{ $cmscontentdiv2['title'] }} @endif</h2>
        @if(isset($cmscontentdiv2)) {!! $cmscontentdiv2['html'] !!} @endif
    </div>
</div>
<div class="container-fluid">
    <div class="why_main_wrap">
        <div class="whyus_side">
            <div class="why_content_left">
                <ul>
                     <li>
                        <span>1</span>
                        <p>Durably constructed with frame corner supports</p>
                    </li>
                    <li>
                        <span>2</span>
                        <p>Pivot Pins allow for bi-directional swing</p>
                    </li>
                    <li>
                        <span>3</span>
                        <p>Bi-metal coil expands/contracts with temperature to open and close louvers. (Dual-Function models only)</p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="why_image_wrap">
            <div class="whyus_image">
                <img src="{{ asset('front_assets/images/whyus_image.png') }}" alt="Image" class="img-fluid">
            </div>
        </div>
        <div class="whyus_side">
            <div class="why_content_left why_right">
                <ul>
                    <li>
                        <span>4</span>
                        <p>316 Marine-Grade Stainless Steel Construction</p>
                    </li>
                    <li>
                        <span>5</span>
                        <p>Door Slots allow water to enter and activate internal floats</p>
                    </li>
                    <li>
                        <span>6</span>
                        <p>3” Debris Clearance meeting ICC-ES Certification</p>
                    </li>
                    <li>
                         <span>7</span>
                        <p>Internal floats for flood door activation</p>
                    </li>
                </ul>
            </div>
         </div>
     </div>

    <div class="why_feature_wrap">
        <div class="why_feature_block">
            <div class="why_image">
               <img src="@if(isset($cmscontentdiv3)) {{ $cmscontentdiv3['image'] }} @endif" alt="Image" class="img-fluid">
            </div>
            <div class="why_feature_info">
                <h3>@if(isset($cmscontentdiv3)) {{ $cmscontentdiv3['title'] }} @endif</h3>
                @if(isset($cmscontentdiv3)) {!! $cmscontentdiv3['html'] !!} @endif
             </div>
        </div>
        <div class="why_feature_block">
            <div class="why_image">
                <img src="@if(isset($cmscontentdiv4)) {{ $cmscontentdiv4['image'] }} @endif" alt="Image" class="img-fluid">
             </div>
            <div class="why_feature_info">
                <h3>@if(isset($cmscontentdiv4)) {{ $cmscontentdiv4['title'] }} @endif</h3>
                @if(isset($cmscontentdiv4)) {!! $cmscontentdiv4['html'] !!} @endif
                
            </div>
        </div>
    </div>

    <div class="why_quote_wrap">
        @if(isset($cmscontentdiv5)) {!! $cmscontentdiv5['html'] !!} @endif
    </div>
    <div class="whyus_services">
        <div class="row">
            @if(isset($cmscontentdiv6))
            @foreach($cmscontentdiv6 as $service)
            <div class="col-md-2">
                <div class="service_block">
                    <img src="{{ $service->image }}" alt="Icon" class="img-fluid">
                    <h2>{{ $service->title }}</h2>
                </div>
            </div>
            @endforeach
            @endif
            <!-- <div class="col-md-2">
                <div class="service_block">
                    <img src="{{ asset('front_assets/images/icon_2.png') }}" alt="Icon" class="img-fluid">
                    <h2>120m sq ft PROTECTION</h2>
                 </div>
            </div>
            <div class="col-md-2">
                 <div class="service_block">
                     <img src="{{ asset('front_assets/images/icon_3.png') }}" alt="Icon" class="img-fluid">
                     <h2>750,000 FLOOD VENTS</h2>
                </div>
              </div>
            <div class="col-md-2">
                <div class="service_block">
                     <img src="{{ asset('front_assets/images/icon_4.png') }}" alt="Icon" class="img-fluid">
                    <h2>FREE FLOOD RISK ANALYSIS</h2>
                </div>
            </div>
            <div class="col-md-2">
                <div class="service_block">
                    <img src="{{ asset('front_assets/images/icon_5.png') }}" alt="Icon" class="img-fluid">
                    <h2>DISASTER SUPPORT</h2>
                 </div>
            </div>
            <div class="col-md-2">
                 <div class="service_block">
                     <img src="{{ asset('front_assets/images/icon_6.png') }}" alt="Icon" class="img-fluid">
                     <h2>COMMUNITY OUTREACH</h2>
                 </div>
            </div> -->

         </div>
    </div>
</div>
</section>
<!-- End -->

<!-- Locator Wrapper -->
<section class="locator_wrapper">
<div class="container">
    <div class="locator_content">
        <img src="{{ asset('front_assets/images/locator_logo.png') }}" alt="Logo" class="img-fluid">
        <h2><h3>@if(isset($cmscontentdiv7)) {!! $cmscontentdiv7['html'] !!} @endif</h3></h2>
        <a href="#" class="btn_3"><span><img src="{{ asset('front_assets/images/locator_icon.png') }}" alt="Icon"></span>Find Your Local Dealer</a>
    </div>
 </div>
</section>
<!-- End -->

<!-- Free Flood Wrapper -->
<section class="flood_wrapper">
<div class="container">
    <div class="free_flood_content">
        <div class="flood_combine">
            <div class="flood_icon">
                <img src="{{ asset('front_assets/images/sub_footer_icon.png') }}" alt="Icon">
            </div>
            <div class="flood_text">
                <h2><span>Free</span> Flood Insurance <span>Evaluation</span></h2>
                <p>OVERPAYING FOR INSURANCE IS SO YESTERDAY</p>
             </div>
        </div>
        <div class="flood_btn">
            <a href="#" class="btn_4">Get Started</a>
        </div>
        <div class="flood_imgs">
            <img src="{{ asset('front_assets/images/sub_footer_sponser_1.png') }}" alt="Image">
            <img src="{{ asset('front_assets/images/sub_footer_sponser_2.png') }}" alt="Image">
        </div>
    </div>
 </div>
</section>
<!-- End -->
@endsection
@section('js')
 <script type="text/javascript">
     img = "@if(isset($silders)){{ $silders['image'] }} @endif";
     var bg = $('.banner_wrap').css('background-image',"url(" + img + ")");
 </script>

@endsection