@extends('layouts.front.app')

@section('og')
    <meta property="og:type" content="home"/>
    <meta property="og:title" content="{{ config('app.name') }}"/>
    <meta property="og:description" content="{{ config('app.name') }}"/>
@endsection 

@section('content')
<!-- ***** Header Area Start ***** -->
     @include('layouts.front.header.header') 
    <!-- ***** Header Area End ***** -->


    <!-- ***** Welcome Area Start ***** -->
     @include('layouts.front.welcome.welcome') 
    <!-- ***** Welcome Area End ***** -->


    <!-- ***** Features Big Item Start ***** -->
     @include('layouts.front.about.about') 
    <!-- ***** Features Big Item End ***** -->

    <!-- ***** Features Small Start ***** -->
    @include('layouts.front.services.services') 
    <!-- ***** Features Small End ***** -->


    <!-- ***** Frequently Question Start ***** -->
    @include('layouts.front.frequentlyQuestions.frequentlyQuestions')
    <!-- ***** Frequently Question End ***** -->
    <!-- ***** Contact Us Start ***** -->
     @include('layouts.front.contact.contact') 
    <!-- ***** Contact Us End ***** -->
@endsection