<!DOCTYPE html>
<html>
<head>
	<title>Candidate approval</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<style type="text/css">
		.font-size{
			font-size: 28px;
		}
		.center_{
			margin-left: 36%;
		}
		@media only screen and (max-width: 425px){
			.center_{
				margin-left: 26%;
			}
		}
	</style>
</head>
<body style="margin: 0; padding: 0;">
 <table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
   <td style="padding: 10px;">
    <table align="center" border="1" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
	 <tr>
	  <td>
	   <table border="0" cellpadding="0" cellspacing="0">
	   	<tr>
			 	<td style="margin-top: 0;display: flex;margin-top: -18px;" class="center_">
			 		<table>
			 			<tr>
			 				<td style="background-color: #96a9a9;height: 65px;width: 160px;">
			 					<table>
			 						<tr>
			 							<td style="background-color: #96a9a9;height: 65px;width: 160px;border: 1px solid gainsboro;"></td>
			 						</tr>
			 					</table>
			 				</td>
			 			</tr>
			 		</table>
			 	</td>
			</tr>
	   	<tr style="margin-top: 100px;">		
	   		<td>
	   			<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
					 <tr>
					  <td style="text-align: center;">
					   <img src="{{env('APP_URL').'/logo.png'}}" alt="Job-Wall" style="width: 75%;">
					  </td>
					 </tr>
					 <tr>
					  <td>
					   <div class="font-size" style="color: #262626;text-align: center;margin-top: 30px;">
							<div>GOOD NEWS!</div>
							<div>YOUR APPLICATION HAS APPROVED AT JOBWALL GLOBAL NOW <br> YOU CAN AMEND YOUR PROFILE AND APPLY FOR JOBS THAT YOU LIKE.</div>
						</div>
					  </td>
					 </tr>
					 <tr>
					  <td>
					    <div style="margin-top: 25px;margin-bottom: 40px; color: #161616;font-size: 18px;text-align: center;">
							<div>For any enquiry please contact us</div>
							<div>Telephone: <a href="tel:+44(0) 207 193 2008" style="text-decoration: none;color: #161616;">+44(0) 207 193 2008</a></div>
							<div>E-mail: <a href="mailto:service.uk@jobwallglobal.com" style="text-decoration: none;color: #161616;">service.uk@jobwallglobal.com</a></div>
						</div>
					  </td>
					 </tr>
					</table>
	   		</td>
	   	</tr>
	   </table>
	  </td>
	 </tr>
	 
	</table>
   </td>
  </tr>
 </table>
</body>
</html>