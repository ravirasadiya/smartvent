=============================================== -->
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
        </div>
        <ul class="sidebar-menu">
            <li><a href="{{ route('admin.dashboard') }}"> <i class="fa fa-home"></i><span> Dashboard</span></a></li>
            
           <!--  @php
                $permission = App\Helper\Permission::permission('company');
            @endphp
            @if($permission->view==1 || $permission->add==1)
                <li class="treeview @if(request()->segment(2) == 'companies') active @endif">
                    <a href="#">
                        <i class="fa fa-building"></i> <span>Companies</span>
                        <span class="pull-right-container">
                                <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @if($permission->view==1)<li><a href="{{ route('admin.companies.index') }}" class="data @if(Request::route()->getName() == 'admin.companies.index') active @endif"><i class="fa fa-circle-o"></i> List Companies</a></li>@endif
                        @if($permission->add==1)<li><a href="{{ route('admin.companies.create') }}" class="@if(Request::route()->getName() == 'admin.companies.create' || Request::route()->getName() == 'admin.companies.edit') active @endif"><i class="fa fa-plus"></i>Add Company</a></li>@endif
                    </ul>
                </li>
            @endif
            @php
                $permission = App\Helper\Permission::permission('category');
            @endphp
            @if($permission->view==1 || $permission->add==1)
            <li class="treeview @if(request()->segment(2) == 'categories') active @endif">
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Categories</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                </a>
                <ul class="treeview-menu">
                    @if($permission->view==1)<li><a href="{{ route('admin.categories.index') }}" class="data @if(Request::route()->getName() == 'admin.categories.index' || Request::route()->getName() == 'admin.categories.show') active @endif"><i class="fa fa-circle-o"></i> List Categories</a></li>@endif
                    @if($permission->add==1)<li><a href="{{ route('admin.categories.create') }}" class="@if(Request::route()->getName() == 'admin.categories.create' || Request::route()->getName() == 'admin.categories.edit') active @endif"><i class="fa fa-plus"></i> Add Category</a></li>@endif
                </ul>
            </li>
            @endif -->
             <!-- @php
                $permission = App\Helper\Permission::permission('crop');
            @endphp
            @if($permission->view==1 || $permission->add==1)
            <li class="treeview @if(request()->segment(2) == 'crops') active @endif">
                <a href="#">
                    <i class="fa fa-gear"></i> <span>Solution</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @if($permission->view==1)<li><a href="{{ route('admin.crops.index') }}" class="data @if(Request::route()->getName() == 'admin.crops.index'|| Request::route()->getName() == 'admin.crops.show') active @endif"><i class="fa fa-circle-o"></i> List Solution</a></li>@endif
                   
                    @if($permission->add==1)<li><a href="{{ route('admin.crops.create', $id=1) }}" class="@if(Request::route()->getName() == 'admin.crops.create') active @endif"><i class="fa fa-plus"></i> Add Sub-Solution</a></li>@endif
                </ul>
            </li>
            @endif
            @php
                $permission = App\Helper\Permission::permission('technical');
            @endphp
            @if($permission->view==1 || $permission->add==1)
            <li class="treeview @if(request()->segment(2) == 'technical') active @endif">
                <a href="#">
                    <i class="fa fa-star-o"></i> <span>Technical Categories</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @if($permission->view==1)<li><a href="{{ route('admin.technical.index') }}" class="data @if(Request::route()->getName() == 'admin.technical.index') active @endif"><i class="fa fa-circle-o"></i> List Technical Categories</a></li>@endif
                    @if($permission->add==1)<li><a href="{{ route('admin.technical.create') }}" class="@if(Request::route()->getName() == 'admin.technical.create' || Request::route()->getName() == 'admin.technical.edit') active @endif"><i class="fa fa-plus"></i> Add Technical Categories</a></li>@endif
                </ul>
            </li>
            @endif
            @php
                $permission = App\Helper\Permission::permission('product');
            @endphp
            @if($permission->view==1 || $permission->add==1)
            <li class="treeview @if(request()->segment(2) == 'products') active @endif">
                <a href="#">
                    <i class="fa fa-envira"></i> <span>Products</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @if($permission->view==1)<li><a href="{{ route('admin.products.index') }}" class="data @if(Request::route()->getName() == 'admin.products.index') active @endif"><i class="fa fa-circle-o"></i> List Products</a></li>@endif
                    @if($permission->add==1)<li><a href="{{ route('admin.products.create') }}" class="@if(Request::route()->getName() == 'admin.products.create' || Request::route()->getName() == 'admin.products.edit') active @endif"><i class="fa fa-plus"></i> Add Product</a></li>@endif
                    <li class="@if(request()->segment(2) == 'attributes') active @endif">
                   
                    </li>
                </ul>
            </li>
            @endif
            @php
                $permission = App\Helper\Permission::permission('order');
            @endphp
            @if($permission->view==1)
            
            <li class="treeview @if(request()->segment(2) == 'orders') active @endif">
                <a href="{{ route('admin.orders.index') }}">
                    <i class="fa fa-money"></i> <span>Orders</span>
                    
                </a>
            </li>
            @endif
            @php
                $permission = App\Helper\Permission::permission('order_status');
            @endphp
            @if($permission->view==1 || $permission->add==1)
            <li class="treeview @if(request()->segment(2) == 'order-statuses') active @endif">
                <a href="{{ route('admin.order-statuses.index') }}">
                    <i class="fa fa-anchor"></i> <span>Order Status</span>
                </a>
                
            </li>
            @endif
            
            @php
                $permission = App\Helper\Permission::permission('customer');
            @endphp
            @if($permission->view==1 || $permission->add==1)
            <li class="treeview @if(request()->segment(2) == 'customers') active @endif">
                <a href="#">
                    <i class="fa fa-user"></i> <span>Customers</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @if($permission->view==1)<li><a href="{{ route('admin.customers.index') }}" class="data @if(Request::route()->getName() == 'admin.customers.index') active @endif"><i class="fa fa-circle-o"></i> List Customers</a></li>@endif
                    @if($permission->add==1)<li><a href="{{ route('admin.customers.create') }}" class="@if(Request::route()->getName() == 'admin.customers.create' || Request::route()->getName() == 'admin.customers.edit') active @endif"><i class="fa fa-plus"></i> Add Customer</a></li>@endif
                </ul>
            </li>
            @endif
            @php
                $permission = App\Helper\Permission::permission('offer');
            @endphp
            @if($permission->view==1 || $permission->add==1)
            <li class="treeview @if(request()->segment(2) == 'offers' || request()->segment(2) == 'offer') active @endif">
                <a href="#">
                    <i class="fa fa-gift"></i> <span>Offers</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @if($permission->view==1)<li><a href="{{ route('admin.offers.index') }}" class="data @if(Request::route()->getName() == 'admin.offers.index') active @endif"><i class="fa fa-circle-o"></i> List Offers</a></li>@endif
                    @if($permission->add==1)<li><a href="{{ route('admin.offers.create') }}" class="@if(Request::route()->getName() == 'admin.offers.create' || Request::route()->getName() == 'admin.offers.edit') active @endif"><i class="fa fa-plus"></i> Add Offer</a></li>@endif
                </ul>
            </li>
            @endif
            @if($user->hasRole('admin|superadmin'))
                @php
                    $permission = App\Helper\Permission::permission('role');
                @endphp
                @if($permission->view==1 || $permission->add==1)
                    <li class="treeview @if(request()->segment(2) == 'roles') active @endif">
                        <a href="#">
                            <i class="fa fa-group"></i> <span>Roles</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-right pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            @if($permission->view==1)<li><a href="{{ route('admin.roles.index') }}" class="data @if(Request::route()->getName() == 'admin.roles.index') active @endif"><i class="fa fa-circle-o"></i> List Roles</a></li>@endif
                            @if($permission->add==1)<li><a href="{{ route('admin.roles.create') }}" class="@if(Request::route()->getName() == 'admin.roles.create' || Request::route()->getName() == 'admin.roles.edit') active @endif"><i class="fa fa-plus"></i> Add Roles</a></li>@endif
                        </ul>
                    </li>
                @endif
            @endif
            @php
                $permission = App\Helper\Permission::permission('priority');
            @endphp
            @if($permission->view==1)
            <li class="treeview @if(request()->segment(2) == 'pro_priority') active @endif">
                <a href="{{ route('admin.pro_priority.index') }}">
                    <i class="fa fa-list"></i> <span>Update Priority</span>
                </a>
                
            </li>
            @endif
            @php
                $permission = App\Helper\Permission::permission('update_stock');
            @endphp
            @if($permission->view==1)
            <li class="treeview @if(request()->segment(2) == 'updateStock') active @endif">
                <a href="{{ route('admin.updateStock.index') }}">
                    <i class="fa fa-edit"></i> <span>Update Stock</span>
                </a>
               
            </li> -->
           <!--  @endif
            @if($user->hasRole('admin|superadmin'))
                @php
                    $permission = App\Helper\Permission::permission('users');
                @endphp
                @if($permission->view==1)
                <li class="treeview @if(request()->segment(2) == 'users') active @endif">
                    <a href="{{ route('admin.users.index') }}">
                        <i class="fa fa-user" aria-hidden="true"></i> <span>Users</span>
                        <span class="pull-right-container">
                                <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                         @if($permission->view==1)<li><a href="{{ route('admin.users.index') }}" class="@if(Request::route()->getName() == 'admin.users.index') active @endif"><i class="fa fa-circle-o"></i> List Users</a></li>@endif
                         @if($permission->add==1)<li><a href="{{ route('admin.users.create') }}" class="@if(Request::route()->getName() == 'admin.users.create' || Request::route()->getName() == 'admin.users.edit') active @endif"><i class="fa fa-circle-o"></i> Add Users</a></li>@endif
                    </ul>
                </li>
                @endif
            @endif
            @php
                $permission = App\Helper\Permission::permission('notification');
            @endphp
            @if($permission->view==1 || $permission->add==1)
            <li class="treeview @if(request()->segment(2) == 'notifications') active @endif">
                <a href="{{ route('admin.notifications.create') }}">
                    <i class="fa fa-bell"></i> <span>Customer Notification</span>
                </a>
                
            </li>
            @endif
            @php
                $permission = App\Helper\Permission::permission('country_data');
            @endphp
            @if($permission->view==1 || $permission->add==1)
            <li class="treeview @if(request()->segment(2) == 'country_data') active @endif">
                <a href="#">
                    <i class="fa fa-bell"></i> <span>Country Data</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @if($permission->view==1)<li><a href="{{ route('admin.country_data.index') }}" class="data @if(Request::route()->getName() == 'admin.country_data.index') active @endif"><i class="fa fa-circle-o"></i> List State</a></li>@endif
                    @if($permission->add==1)<li><a href="{{ route('admin.country_data.createCity') }}" class=data "@if(Request::route()->getName() == 'admin.country_data.createCity') active @endif"><i class="fa fa-plus"></i> Add District</a></li>@endif
                    @if($permission->add==1)<li><a href="{{ route('admin.country_data.create') }}" class="data @if(Request::route()->getName() == 'admin.country_data.create') active @endif"><i class="fa fa-plus"></i> Add Tehsil</a></li>@endif
                    @if($permission->add==1)<li><a href="{{ route('admin.country_data.createVillage') }}" class="@if(Request::route()->getName() == 'admin.country_data.createVillage') active @endif"><i class="fa fa-plus"></i> Add Village</a></li>@endif
                </ul>
            </li>
            @endif
            @php
                $permission = App\Helper\Permission::permission('wallet');
            @endphp
            @if($permission->view==1)
            <li class="treeview @if(request()->segment(2) == 'wallet') active @endif">
                <a href="{{ route('admin.wallet.index') }}">
                    <i class="fa fa-credit-card"></i> <span> Wallet</span>
                </a>
            </li>
             @endif -->
          
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Silders</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                </a>
                <ul class="treeview-menu">
                  
                   <li><a href="{{ route('admin.silders.index') }}" >
                    <i class="fa fa-circle-o"></i> List Silders</a></li>
                   
                    <li><a href="{{ route('admin.silders.create') }}" ><i class="fa fa-plus"></i> Add Silder</a></li>
                    
                </ul>
            </li>
            
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Home page Content</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <!-- <li><a href="{{ route('admin.services.index') }}" >
                        <i class="fa fa-circle-o"></i> List CMS Content</a>
                    </li> -->
                   <li><a href="{{ route('admin.cmscontent.createfloodvent') }}" >
                     <i class="fa fa-plus"></i> Flood Vents</a>
                  </li>
                  <li><a href="{{ route('admin.cmscontent.createsmartvent') }}" >
                     <i class="fa fa-plus"></i> Why Smart Vent</a>
                  </li>
                  <li><a href="{{ route('admin.cmscontent.createresidentl') }}" >
                     <i class="fa fa-plus"></i> Residential </a>
                  </li>
                  <li><a href="{{ route('admin.cmscontent.createcomrcl') }}" >
                     <i class="fa fa-plus"></i> Commercial  </a>
                  </li>
                  <li><a href="{{ route('admin.cmscontent.createsmartventprodcts') }}" >
                     <i class="fa fa-plus"></i> Smart Vent Products  </a>
                  </li>
                  <li><a href="{{ route('admin.cmscontent.createservices') }}" >
                     <i class="fa fa-plus"></i>Services  </a>
                  </li>
                  <li><a href="{{ route('admin.cmscontent.createlookingforsmrtvnt') }}" >
                     <i class="fa fa-plus"></i>Looking For smart vent  </a>
                  </li>
                </ul>
                
            </li>
            
           @php
                $permission = App\Helper\Permission::permission('customer');
            @endphp
            @if($permission->view==1 || $permission->add==1)
            <li class="treeview @if(request()->segment(2) == 'customers') active @endif">
                <a href="#">
                    <i class="fa fa-user"></i> <span>Customers</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @if($permission->view==1)<li><a href="{{ route('admin.customers.index') }}" class="data @if(Request::route()->getName() == 'admin.customers.index') active @endif"><i class="fa fa-circle-o"></i> List Customers</a></li>@endif
                    @if($permission->add==1)<li><a href="{{ route('admin.customers.create') }}" class="@if(Request::route()->getName() == 'admin.customers.create' || Request::route()->getName() == 'admin.customers.edit') active @endif"><i class="fa fa-plus"></i> Add Customer</a></li>@endif
                </ul>
            </li>
            @endif
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- ===============================================