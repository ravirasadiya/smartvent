<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <!--<b>Version</b> 0.1.0-->
    </div>
    <strong>Copyright &copy; {{ date('Y') }} - {{ date('Y') + 1 }} {{config('app.name')}}.</strong> All rights
    reserved.
</footer>