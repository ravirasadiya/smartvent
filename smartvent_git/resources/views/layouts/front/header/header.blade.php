            <!-- Header Block -->
            <header class="main_header_wrap">
                <div class="top-header">
                    <div class="container">
                        <div class="top_main_wrap">
                            <div class="top-buttons">
                                <p><span><img src="{{ asset('front_assets/images/header_location.png') }}" alt="Icon" class="img-fluid"></span><a href="#">DEALER LOCATOR</a></p>
                                <p><span><img src="{{ asset('front_assets/images/header_register.png') }}" alt="Icon" class="img-fluid"></span><a href="#">PRODUCT REGISTRATION</a></p>
                            </div>
                            <div class="top-social">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>                      
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bottom-header">
                    <div class="container">
                        <nav class="navbar navbar-expand-lg">
                            <a class="navbar-brand" href="#">
                                <img src="{{ asset('front_assets/images/logo.png') }}" alt="Logo" class="img-fluid">
                            </a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                                <span class="navbar-toggler-icon"></span>
                                <span class="navbar-toggler-icon"></span>                                
                            </button>
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item active">
                                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Product Catalog</a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="#">Product Catalog 1</a>
                                            <a class="dropdown-item" href="#">Product Catalog 2</a>
                                            <a class="dropdown-item" href="#">Product Catalog 3</a>
                                            <a class="dropdown-item" href="#">Product Catalog 4</a>
                                        </div>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Professional Services</a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                            <a class="dropdown-item" href="#">Professional Services 1</a>
                                            <a class="dropdown-item" href="#">Professional Services 2</a>
                                            <a class="dropdown-item" href="#">Professional Services 3</a>
                                            <a class="dropdown-item" href="#">Professional Services 4</a>
                                        </div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Resources</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Portfolio</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Contact Us</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </header>
            <!-- End -->