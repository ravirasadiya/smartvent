<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon -->
        <link rel="icon" type="image/png" href="{{ asset('front_assets/images/favicon.png' ) }}"/>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ asset('front_assets/css/bootstrap.min.css' ) }}">
        <!-- Reset CSS -->
        <link rel="stylesheet" href="{{ asset('front_assets/css/reset.css' ) }}">
        <!-- Custom CSS -->
        <link rel="stylesheet" type="text/css" href="{{ asset('front_assets/css/style.css' ) }}">
        <!-- Responsive CSS -->
        <link rel="stylesheet" type="text/css" href="{{ asset('front_assets/css/media.css' ) }}">
        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="{{ asset('front_assets/font-awesome/css/font-awesome.min.css' ) }}">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/assets/owl.carousel.min.css">
         @yield('css')
        <title>{{ config('app.name') }}</title>
    </head>
    <body>
        <main class="main_homepage_wrapper">

<!-- Header Block -->
 @include('layouts.front.header.header') 
<!-- End -->
 @yield('content')
          
<!-- Footer -->
@include('layouts.front.footer.footer') 
<!-- End -->
            
        </main>
        
        <!-- jQuery first, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="{{ asset('front_assets/js/bootstrap.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.min.js"></script>
        @yield('js')
    </body>
</html>
