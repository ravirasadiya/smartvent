    <!-- ***** Features Small Start ***** -->
    <section class="section" id="services">
        <div class="container">
            <div class="row">
                <div class="owl-carousel owl-theme">
                    <div class="item service-item">
                        <div class="icon">
                            <i><img src="{{ asset('front_assets/images/service-icon-01.png') }}" alt=""></i>
                        </div>
                        <h5 class="service-title">What is Lorem Ipsum?</h5>
                        <p>From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage.</p>
                        <a href="#" class="main-button">Read More</a>
                    </div>
                    <div class="item service-item">
                        <div class="icon">
                            <i><img src="{{ asset('front_assets/images/service-icon-02.png') }}" alt=""></i>
                        </div>
                        <h5 class="service-title">What is Lorem Ipsum?</h5>
                        <p>From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage. </p>
                        <a href="#" class="main-button">Discover More</a>
                    </div>
                    <div class="item service-item">
                        <div class="icon">
                            <i><img src="{{ asset('front_assets/images/service-icon-03.png') }}" alt=""></i>
                        </div>
                        <h5 class="service-title">What is Lorem Ipsum?</h5>
                        <p>From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage.</p>
                        <a href="#" class="main-button">More Detail</a>
                    </div>
                    <div class="item service-item">
                        <div class="icon">
                            <i><img src="{{ asset('front_assets/images/service-icon-02.png') }}" alt=""></i>
                        </div>
                        <h5 class="service-title">What is Lorem Ipsum?</h5>
                        <p>From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage.</p>
                        <a href="#" class="main-button">Read More</a>
                    </div>
                    <div class="item service-item">
                        <div class="icon">
                            <i><img src="{{ asset('front_assets/images/service-icon-01.png') }}" alt=""></i>
                        </div>
                        <h5 class="service-title">What is Lorem Ipsum?</h5>
                        <p>From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage.</p>
                        <a href="#" class="main-button">Discover</a>
                    </div>
                    <div class="item service-item">
                        <div class="icon">
                            <i><img src="{{ asset('front_assets/images/service-icon-03.png') }}" alt=""></i>
                        </div>
                        <h5 class="service-title">What is Lorem Ipsum?</h5>
                        <p>From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage.</p>
                        <a href="#" class="main-button">Detail</a>
                    </div>
                    <div class="item service-item">
                        <div class="icon">
                            <i><img src="{{ asset('front_assets/images/service-icon-01.png') }}" alt=""></i>
                        </div>
                        <h5 class="service-title">What is Lorem Ipsum?</h5>
                        <p>From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage.</p>
                        <a href="#" class="main-button">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Small End ***** -->