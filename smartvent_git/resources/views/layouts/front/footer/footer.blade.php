            <!-- Footer -->
            <footer class="footer_wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col mob_hde">
                            <div class="footer_links_block">
                                <h3 class="footer_title">About Us</h3>
                                <ul>
                                    <li><a href="#">Why Smartvent?</a></li>
                                    <li><a href="#">Meet the Team</a></li>
                                    <li><a href="#">Portfolio</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col mob_hde">
                            <div class="footer_links_block">
                                <h3 class="footer_title">Product Catalog</h3>
                                <ul>
                                    <li><a href="#">Dual Function Series</a></li>
                                    <li><a href="#">Insulated Series</a></li>
                                    <li><a href="#">Options & Accessories</a></li>
                                    <li><a href="#">Dry Floodproofing</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col mob_hde">
                            <div class="footer_links_block">
                                <h3 class="footer_title">Professional Services</h3>
                                <ul>
                                    <li><a href="#">Plans Support</a></li>
                                    <li><a href="#">Education/Webinars</a></li>
                                    <li><a href="#">Flood Vent Facts</a></li>
                                    <li><a href="#">Codes</a></li>
                                    <li><a href="#">FAQs</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col">
                            <div class="footer_links_block footer_contact">
                                <h3 class="footer_title">Contact Us</h3>
                                <div class="con_foot">
                                    <span>877) 441-8368</span>
                                    <p>430 Andbro Drive, Unit 1 <br/>Pitman, NJ 08071</p>
                                </div>
                                <div class="top-buttons">
                                    <p><span><img src="{{ asset('front_assets/images/header_location.png') }}" alt="Icon" class="img-fluid"></span><a href="#">DEALER LOCATOR</a></p>
                                    <p><span><img src="{{ asset('front_assets/images/header_register.png') }}" alt="Icon" class="img-fluid"></span><a href="#">PRODUCT REGISTRATION</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="footer_info">
                                <div class="footer_logo">
                                    <a href="#"><img src="{{ asset('front_assets/images/footer_logo.png') }}" alt="Logo" class="img-fluid"></a>
                                </div>
                                <div class="top-social">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>                      
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer_copyright">
                        <p>© 2020 Smartvent, all rights reserved.</p>
                    </div>
                </div>
            </footer>
            <!-- End -->