    <!-- ***** Frequently Question Start ***** -->
    <section class="section" id="frequently-question">
        <div class="container">
            <!-- ***** Section Title Start ***** -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-heading">
                        <h2>What is Lorem Ipsum?</h2>
                    </div>
                </div>
                <div class="offset-lg-3 col-lg-6">
                    <div class="section-heading">
                        <p>From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage.</p>
                    </div>
                </div>
            </div>
            <!-- ***** Section Title End ***** -->

            <div class="row">
                <div class="left-text col-lg-6 col-md-6 col-sm-12">
                    <h5>{{ config('app.name') }} Pvt. Ltd.,</h5>
                    <div class="accordion-text">
                        <p>B-89, Marketing Yard( Bedii), Morbi Road, Rajkot- 360003</p>
                        <p>Tel: <a href="tel:020-42115622" class="text-275540">020-42115622</a> | <a href="tel:020-42115612" class="text-275540">020-42115612</a></p>
                        <span>Email: <a href="mailto:khedutbolo@gmail.com" class="text-275540">khedutbolo@gmail.com</a><br></span>
                        <a href="#contact-us" class="main-button">Contact Us</a>
                    </div> 
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="accordions is-first-expanded">
                        <article class="accordion">
                            <div class="accordion-head">
                                <span>What is Lorem Ipsum?</span>
                                <span class="icon">
                                    <i class="icon fa fa-chevron-right"></i>
                                </span>
                            </div>
                            <div class="accordion-body">
                                <div class="content">
                                    <p>From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage.
                                    <br><br>
                                    From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage.</p>
                                </div>
                            </div>
                        </article>
                        <article class="accordion">
                            <div class="accordion-head">
                                <span>What is Lorem Ipsum?</span>
                                <span class="icon">
                                    <i class="icon fa fa-chevron-right"></i>
                                </span>
                            </div>
                            <div class="accordion-body">
                                <div class="content">
                                    <p>From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage.
                                    <br><br>
                                    From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage.</p>
                                </div>
                            </div>
                        </article>
                        <article class="accordion">
                            <div class="accordion-head">
                                <span>What is Lorem Ipsum?</span>
                                <span class="icon">
                                    <i class="icon fa fa-chevron-right"></i>
                                </span>
                            </div>
                            <div class="accordion-body">
                                <div class="content">
                                    <p>From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage. 
                                    <br><br>
                                    From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage.</p>
                                </div>
                            </div>
                        </article>
                        <article class="accordion">
                            <div class="accordion-head">
                                <span>What is Lorem Ipsum?</span>
                                <span class="icon">
                                    <i class="icon fa fa-chevron-right"></i>
                                </span>
                            </div>
                            <div class="accordion-body">
                                <div class="content">
                                    <p>From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage.
                                    <br><br>
                                    From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage.</p>
                                </div>
                            </div>
                        </article>
                        <article class="accordion">
                            <div class="accordion-head">
                                <span>What is Lorem Ipsum?</span>
                                <span class="icon">
                                    <i class="icon fa fa-chevron-right"></i>
                                </span>
                            </div>
                            <div class="accordion-body">
                                <div class="content">
                                    <p>From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage.
                                    <br><br>
                                    From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage. </p>
                                </div>
                            </div>
                        </article>
                        <article class="accordion">
                            <div class="accordion-head">
                                <span>What is Lorem Ipsum?</span>
                                <span class="icon">
                                    <i class="icon fa fa-chevron-right"></i>
                                </span>
                            </div>
                            <div class="accordion-body">
                                <div class="content">
                                    <p>From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage.</p>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Frequently Question End ***** -->