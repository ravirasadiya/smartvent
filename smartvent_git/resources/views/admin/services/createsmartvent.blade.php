@extends('layouts.admin.app')
@section('content')
    <!-- Main content -->
    <section class="content">
        <!-- @include('layouts.errors-and-messages') -->
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">Why Smart Vent Description</h4>
                    </div>
                </div>
                    <div class="box-body">
                    <form action="{{ route('admin.cmscontent.storesmartvent') }}" method="post" class="form" enctype="multipart/form-data" id="add_smartVent">
                            {{ csrf_field() }}
                             <div class="row">
                                <input type="hidden" name="page_id" value="1">
                                <input type="hidden" name="div_id" value="2">
                                <!-- <div class="col-lg-6">
                                    <h3 class="page-title header-color">Why Smart Vent Description</h3>
                                </div> -->
                                </div>
                                <div class="row">
                                <div class="col-lg-6">
                                  <div class="form-group">
                                    <label for="offer_product_image">Why Smart Vent Title <span class="text-danger">*</span></label>
                                      <input type="text" name="title" id="title" placeholder="Why Smart Vent Title" class="form-control character" maxlength="25" value="{{$title}}">
                                    @if ($errors->has('name'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('title') }}</span>
                                    @endif
                                  </div>
                                  <div class="form-group">
                                    <label for="offer_product_image">Why Smart Vent Description <span class="text-danger">*</span></label>
                                      <textarea class="form-control" name="description" id="description" rows="5" placeholder="Description">{{$description}}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('description') }}</span>
                                    @endif
                                  </div>
                                  <div class="box-footer csm-service">
                                   <div class="btn-group">
                                     <a href="{{ route('admin.services.index') }}" class="btn btn-default">Cancel</a>
                            
                                      <button type="submit" class="btn btn-primary" id="add_submit">Save</button>
                                   </div>
                                </div>
                                </div>
                                </div>
                                
                            </div>
                        </form>
                       
                    <!-- /.box-body -->
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection


@section('js')

<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script>
    $(document).ready(function(){
         var i=1;
        $('#addresidentl').click(function(){
            i++;
            $("#createresident").append('<span id="createresident'+i+'"><div class="form-group"><input type="file" name="image[]" id="image[]" class="form-control" accept="image/*" onchange="GetFileSize()"><small class="text-warning">Note :- Upload image with .jpg, .jpeg and .png extensions upto 1 MB.</small><label id="image" class="image">Please select cover image</label><label id="image_error" class="image_error">Image size is large.</label><label id="image_type_error"  class="image_type_error">Select image only .jpg, .png, .jpeg file</label></div><div class="form-group"><input type="text" name="title[]" id="title[]" placeholder="Residentia Title" class="form-control character" maxlength="25"></div> <textarea class="form-control ckeditor" name="description[]" id="description[]" rows="5" placeholder="Description"></textarea><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove" style="margin-top: 24px;">x</button></span>');

            //$('.ckeditor').ckeditor();
            //$("#createresident").find('textarea').ckeditor();
           // var createevent= $("#createresident").html("remove");
            //$("#createresident").clone().html('dfd').appendTo(createevent);
        });
        $(document).on('click', '.btn_remove', function(){  
            var button_id = $(this).attr("id");   
            $('#createresident'+button_id+'').remove();  
        });
    });
</script>
<script type="text/javascript">
    @if(session()->has('message'))
      toastr.success("{{ session()->get('message') }}");
    @elseif(session()->has('error'))
      toastr.error("{{ session()->get('error') }}");
    @endif
</script>
@endsection

