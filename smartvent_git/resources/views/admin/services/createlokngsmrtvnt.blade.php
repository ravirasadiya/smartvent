@extends('layouts.admin.app')
@section('content')
    <!-- Main content -->
    <section class="content">
        <!-- @include('layouts.errors-and-messages') -->
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">Add CMS Content</h4>
                    </div>
                </div>
                    <div class="box-body">
                    <form action="{{ route('admin.cmscontent.storelokngsmrtvnt') }}" method="post" class="form" enctype="multipart/form-data" id="add_smartVentProducts">
                            {{ csrf_field() }}
                            <div class="row">
                                <input type="hidden" name="page_id" value="1">
                                <input type="hidden" name="div_id" value="5">
                                <div class="col-lg-6">
                                    <h3 class="page-title header-color">Smart Vent Products Description</h3>
                                    <p>Smart Vent Products, Inc. was born out of the need to create flood vents —a safeguard mandated by the Federal Emergency Management Agency, the National Flood Insurance Program, and State and National Construction Codes.</p>
                                </div>
                                <div class="col-lg-6">
                                     <label for="offer_product_image">Smart Vent  Description<span class="text-danger">*</span></label>
                                    <textarea class="form-control ckeditor" name="description" id="description" rows="5" placeholder="Description">{{ old('description') }}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('description') }}</span>
                                    @endif
                                </div>
                                <div class="box-footer csm-service">
                                   <div class="btn-group">
                                     <a href="{{ route('admin.services.index') }}" class="btn btn-default">Cancel</a>
                            
                                      <button type="submit" class="btn btn-primary" id="add_submit">Save</button>
                                   </div>
                                </div>
                            </div>
                        </form>
                    <!-- /.box-body -->
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection


@section('js')

<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>

<script type="text/javascript">
    @if(session()->has('message'))
      toastr.success("{{ session()->get('message') }}");
    @elseif(session()->has('error'))
      toastr.error("{{ session()->get('error') }}");
    @endif
</script>
@endsection

