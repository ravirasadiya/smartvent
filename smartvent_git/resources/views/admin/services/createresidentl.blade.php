@extends('layouts.admin.app')
@section('content')
    <!-- Main content -->
    <section class="content">
        <!-- @include('layouts.errors-and-messages') -->
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">Residential Description</h4>
                    </div>
                </div>
                    <div class="box-body">
                        <form action="{{ route('admin.cmscontent.storeresidentl') }}" method="post" class="form" enctype="multipart/form-data" id="add_residential">
                            {{ csrf_field() }}
                            <div class="row">
                                <input type="hidden" name="page_id" value="1">
                                <input type="hidden" name="div_id" value="3">
                                <!-- <div class="col-lg-6">
                                    <h3 class="page-title header-color">Residential Description</h3>
                                    <p>Enclosed areas are permitted under elevated buildings if they meet use and construction requirements.</p>
                                </div> -->
                                </div>
                                <div class="row">
                                <div class="col-lg-6">
                                @if($cmscontentdiv3->count()==0)
                                    <span id="createresident">
                                            <input type="hidden" name="mainid[]" id="mainid[]"/>
                                            <div class="form-group">
                                                <label for="offer_product_image">Residential Image  <span class="text-danger">*</span></label>
                                                <input type="file" name="image[]" id="image[]" class="form-control" accept="image/*" onchange="GetFileSize()">
                                                <small class="text-warning">Note :- Upload image with .jpg, .jpeg and .png extensions upto 1 MB.(best size 153 * 177)</small>
                                                <label id="image" class="image">Please select cover image</label>
                                                <label id="image_error" class="image_error">Image size is large.</label>
                                                <label id="image_type_error"  class="image_type_error">Select image only .jpg, .png, .jpeg file</label><br/>
                                        </div>
                                            <div class="form-group">
                                                <label for="offer_product_image">Header Text 1st <span class="text-danger">*</span></label>
                                            <input type="text" name="title[]" id="title[]" placeholder="Residentia Title" class="form-control character" maxlength="25">
                                            @if ($errors->has('name'))
                                                <span class="text-danger" style="color: red">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                        <textarea class="form-control" name="description[]" id="description[]" rows="5" placeholder="Description"></textarea> 
                                        </span>
                                    @endif
                                    @if($cmscontentdiv3->count()>0)
                                    @foreach($editcontentdiv as $key=>$cont)
                                     <span id="createresident">
                                        <input type="hidden" name="mainid[]" id="mainid[]" value="{{$cont->id}}"/>
                                         <div class="form-group">
                                            <label for="offer_product_image">Residential Image  <span class="text-danger">*</span></label>
                                            <input type="file" name="image[]" id="image[]" class="form-control" accept="image/*" onchange="GetFileSize()">
                                            <small class="text-warning">Note :- Upload image with .jpg, .jpeg and .png extensions upto 1 MB. (best size 153 * 177)</small>
                                            <label id="image" class="image">Please select cover image</label>
                                            <label id="image_error" class="image_error">Image size is large.</label>
                                            <label id="image_type_error"  class="image_type_error">Select image only .jpg, .png, .jpeg file</label><br/>
                                            @if($cont->image!='')
                                            <img src="{{url($cont->image)}}" height="100"/>
                                            @endif
                                      </div>
                                        <div class="form-group">
                                            <label for="offer_product_image">Residential Title  <span class="text-danger">*</span></label>
                                        <input type="text" name="title[]" id="title[]" placeholder="Residentia Title" class="form-control character" maxlength="25" value="{{$cont->title}}">
                                        @if ($errors->has('name'))
                                            <span class="text-danger" style="color: red">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                     <label for="offer_product_image">Residential Description  <span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="description[]" id="description[]" rows="5" placeholder="Description">{{$cont->html}}</textarea> 
                                    @if($key!=0)
                                    <a name="remove" href="{{route('admin.cmscontent.removeresidentl',['id'=>$cont->id])}}" class="btn btn-danger btn_remove" style="margin: -184px -76px 4px 19px;float:right;">x</a>
                                    @endif
                                    @if ($errors->has('description'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('description') }}</span>
                                    @endif
                                    </span>
                                    @endforeach
                                @endif
                                <span id="editresident"></span>
                                    <div class="box-footer csm-service">
                                   <div class="btn-group">
                                     <a href="{{ route('admin.services.index') }}" class="btn btn-default">Cancel</a>
                            
                                      <button type="submit" class="btn btn-primary" id="add_submit">Save</button>
                                   </div>
                                </div>
                                </div>
                                <div class="col-lg-1"><button type="button" name="addresidentl" id="addresidentl" class="btn btn-success text-right" style="margin-top: 24px;float:right;">+</button></div>
                                
                            </div>
                        </form>
                       
                    <!-- /.box-body -->
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection


@section('js')

<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script>
function deleteResntl(id){
    
}
</script>
<script>
    $(document).ready(function(){
         var i=1;
        $('#addresidentl').click(function(){
            i++;
            $("#editresident").append('<span id="createresident'+i+'"><div class="form-group"><input type="hidden" name="mainid[]" id="mainid[]"/><input type="file" name="image[]" id="image[]" class="form-control" accept="image/*" onchange="GetFileSize()"><small class="text-warning">Note :- Upload image with .jpg, .jpeg and .png extensions upto 1 MB.</small><label id="image" class="image">Please select cover image</label><label id="image_error" class="image_error">Image size is large.</label><label id="image_type_error"  class="image_type_error">Select image only .jpg, .png, .jpeg file</label></div><div class="form-group"><input type="text" name="title[]" id="title[]" placeholder="Residentia Title" class="form-control character" maxlength="25"></div> <textarea class="form-control ckeditor" name="description[]" id="description[]" rows="5" placeholder="Description"></textarea><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove" style="margin: -184px -76px 4px 19px;float:right;">x</button></span>');

            //$('.ckeditor').ckeditor();
            //$("#createresident").find('textarea').ckeditor();
           // var createevent= $("#createresident").html("remove");
            //$("#createresident").clone().html('dfd').appendTo(createevent);
        });
        $(document).on('click', '.btn_remove', function(){  
            var button_id = $(this).attr("id");   
            $('#createresident'+button_id+'').remove();  
        });
    });
</script>
<script type="text/javascript">
    @if(session()->has('message'))
      toastr.success("{{ session()->get('message') }}");
    @elseif(session()->has('error'))
      toastr.error("{{ session()->get('error') }}");
    @endif
</script>
@endsection

