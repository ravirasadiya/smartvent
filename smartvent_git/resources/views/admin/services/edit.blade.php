@extends('layouts.admin.app')
@section('content')
    <!-- Main content -->
    <section class="content">
        <!-- @include('layouts.errors-and-messages') -->
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">Edit Category</h4>
                    </div>
                </div>
               
                <form action="{{ route('admin.services.update', $services->id) }}" method="post" class="form" enctype="multipart/form-data" id="edit_category">
                    <div class="box-body">
                        <input type="hidden" name="_method" value="put">
                        {{ csrf_field() }}
                        
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="description">Description </label>
                                    <textarea class="form-control ckeditor" name="description" id="description" rows="5" placeholder="Description">
                                        {!! $services->description ?: old('description')  !!}
                                    </textarea>
                                    @if ($errors->has('description'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('description') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="image">Image <span class="text-danger">*</span></label>
                                    <input type="file" name="image" id="image" class="form-control" accept="image/*" onchange="GetFileSize()">
                                    <small class="text-warning">Note :- Upload image with .jpg, .jpeg and .png extensions upto 1 MB.</small>
                                    <label id="image" class="image">Please select cover image</label>
                                    <label id="image_error" class="image_error">Image size is large.</label>
                                    <label id="image_type_error"  class="image_type_error">Select image only .jpg, .png, .jpeg file</label>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="status">Status <span class="text-danger">*</span></label>
                                    <select name="status" id="status" class="form-control">
                                        <option value="0" @if($services->status == 0) selected="selected" @endif>Disable</option>
                                        <option value="1" @if($services->status == 1) selected="selected" @endif>Enable</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>
                       @if(isset($services->image))
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group" style="margin-top: 10px;">
                                    <img style="height:100px !important" src="{{ $services->image }}" alt="image not fount" class="img-responsive" height="200px"/> <br/>
                                </div>
                            </div>
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>
                        @endif
                       
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <a href="{{ route('admin.categories.index') }}" class="btn btn-default">Cancel</a>
                            <input type="button" class="btn btn-info" value="Translate" id="translate">
                            <button type="submit" class="btn btn-primary" id="edit_submit">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
@section('js')

<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>

<script type="text/javascript">
    $("#edit_category").validate({
        ignore: [],
        errorClass: 'error text-change',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
            name: {
                required: true,
                maxlength: 25,
            },
            company_id : {
                required: true,
            },
            status : {
                required: true,
            }
        },
        messages: {
            "name":{
                required: "Please enter name",
                maxlength:"Please enter no more than 25 character",
            },
            "company_id":{
                required: "Select company name",
            },
            "status":{
                required: "Select status",
            }
        },
            
        submitHandler: function (form) {
             error=0;
            document.getElementById('image_error').style.display="none";
            document.getElementById('image_type_error').style.display="none";
            document.getElementById('image').style.display="none";
            var logo = $('#cover').val();
            console.log(logo);
            if(logo!=""){
                var fi = document.getElementById('cover');
                if (fi.files.length > 0) {
                    for (var i = 0; i <= fi.files.length - 1; i++) {
                        var fsize = fi.files.item(i).size;
                        var type = fi.files.item(i).type;
                        var size = fsize/1024;   
                        console.log('fsize'+fsize+' type '+type+' size '+size);
                        if(!(type=='image/png'||type=='image/jpeg'||type=='image/jpg')){
                            document.getElementById('image_type_error').style.display="block";
                            error = 1;
                        } else if(size>1024){
                            document.getElementById('image_error').style.display="block";
                            error = 1;
                        }
                    }
                } 
            }
            if (error == 1)  {
                return false;
            }
            $('#edit_submit').attr('disabled', true);
            form.submit();
        },
    });
    function GetFileSize() {
        var fi = document.getElementById('cover');
        document.getElementById('image_error').style.display="none";
        document.getElementById('image_type_error').style.display="none";
        if (fi.files.length > 0) {
            for (var i = 0; i <= fi.files.length - 1; i++) {

                var fsize = fi.files.item(i).size;
                var type = fi.files.item(i).type;
                var size = fsize/1024; 
                if(!(type=='image/png'||type=='image/jpeg'||type=='image/jpg')){
                    document.getElementById('image_type_error').style.display="block";
                } else if(size>1024){
                    document.getElementById('image_error').style.display="block";
                }
            }
        }
    }
    
    $('.character').on('input', function (event) {
        this.value = this.value.replace(/[^0-9\.\a-z\A-Z\@ _()$&]/g, '');
    });

    $("#translate").on('click',function(){
        $("#add_submit").css('display','block');
        var name = $("#name").val();
        var description = CKEDITOR.instances['description'].getData();
        console.log(description);
        $(".box").css({'opacity':'0.5','pointer-events': 'none'});
        $.ajax({
            url : "{{route('admin.translate')}}",
            data: { "name": name,"description":description },
            type: 'get',
            dataType: 'json',
            success: function( result )
            {
                console.log(result);
                var jsonObj = result;
                $("#name_hi").val(jsonObj.name.hi);
                $("#name_gu").val(jsonObj.name.gu);
                CKEDITOR.instances['description_hi'].setData(jsonObj.description.hi);
                CKEDITOR.instances['description_gu'].setData(jsonObj.description.gu);
                // console.log(result);
                $(".box").css({'opacity':'1','pointer-events': 'auto'});
            },
            error: function(error)
            {
                console.log(error);
            }
        });
    });
</script>
@endsection

