@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        <!-- @include('layouts.errors-and-messages') -->
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">Add CMS Content</h4>
                    </div>
                </div>
                    <div class="box-body">
                        {{ csrf_field() }}
                        <form action="{{ route('admin.cmscontent.store') }}" method="post" class="form" enctype="multipart/form-data" id="add_floodVent">
                            {{ csrf_field() }}
                            <div class="row">
                                <input type="hidden" name="page_id" id="page_id" value="1">
                                <input type="hidden" name="div_id" id="div_id" value="1">
                                <div class="col-lg-6">
                                    <h3 class="page-title header-color">Flood Vents Description</h3>
                                    <p>Flood Vents protect your foundation by allowing bidirectional water flow that relieves hydrostatic pressure.</p>
                                </div>
                                <div class="col-lg-6">
                                    <textarea class="form-control ckeditor" name="description" id="description" rows="5" placeholder="Description">{{ old('description') }}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('description') }}</span>
                                    @endif
                                </div>
                                <div class="box-footer csm-service">
                                   <div class="btn-group">
                                     <a href="{{ route('admin.services.index') }}" class="btn btn-default">Cancel</a>
                            
                                      <button type="submit" class="btn btn-primary">Save</button>
                                   </div>
                                </div>
                            </div>
                        </form>
                        <form action="{{ route('admin.cmscontent.store') }}" method="post" class="form" enctype="multipart/form-data" id="add_smartVent">
                            {{ csrf_field() }}
                             <div class="row">
                                <input type="hidden" name="page_id" value="1">
                                <input type="hidden" name="div_id" value="2">
                                <div class="col-lg-6">
                                    <h3 class="page-title header-color">Why Smart Vent Description</h3>
                                    <p>The SMART VENT door is latched closed until flood water is present. Rising flood water activates the internal floats, which unlatch and open the door. Smart Vent’s 3-inch clearance, when open, helps let flood debris pass through unlike a typical air vent that will clog.</p>
                                </div>
                                <div class="col-lg-6">
                                  <div class="form-group">
                                      <input type="text" name="title" id="title" placeholder="Why Smart Vent Title" class="form-control character" maxlength="25">
                                    @if ($errors->has('name'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('name') }}</span>
                                    @endif
                                  </div>
                                  <div class="form-group">
                                      <textarea class="form-control ckeditor" name="description" id="description" rows="5" placeholder="Description">{{ old('description') }}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('description') }}</span>
                                    @endif
                                  </div>
                                </div>
                                <div class="box-footer csm-service">
                                   <div class="btn-group">
                                     <a href="{{ route('admin.services.index') }}" class="btn btn-default">Cancel</a>
                            
                                      <button type="submit" class="btn btn-primary" id="add_submit">Save</button>
                                   </div>
                                </div>
                            </div>
                        </form>
                        <form action="{{ route('admin.cmscontent.store') }}" method="post" class="form" enctype="multipart/form-data" id="add_residential">
                            {{ csrf_field() }}
                            <div class="row">
                                <input type="hidden" name="page_id" value="1">
                                <input type="hidden" name="div_id" value="3">
                                <div class="col-lg-6">
                                    <h3 class="page-title header-color">Residential Description</h3>
                                    <p>Enclosed areas are permitted under elevated buildings if they meet use and construction requirements.</p>
                                </div>

                                
                                <div class="col-lg-6">
                                     <span id="createresident">
                                         <div class="form-group">
                                            <input type="file" name="image[]" id="image[]" class="form-control" accept="image/*" onchange="GetFileSize()">
                                            <small class="text-warning">Note :- Upload image with .jpg, .jpeg and .png extensions upto 1 MB.</small>
                                            <label id="image" class="image">Please select cover image</label>
                                            <label id="image_error" class="image_error">Image size is large.</label>
                                            <label id="image_type_error"  class="image_type_error">Select image only .jpg, .png, .jpeg file</label>
                                      </div>
                                        <div class="form-group">
                                        <input type="text" name="title[]" id="title[]" placeholder="Residentia Title" class="form-control character" maxlength="25">
                                        @if ($errors->has('name'))
                                            <span class="text-danger" style="color: red">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                    <textarea class="form-control ckeditor" name="description[]" id="description[]" rows="5" placeholder="Description"></textarea>
                                    @if ($errors->has('description'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('description') }}</span>
                                    @endif
                                    <button type="button" name="addresidentl" id="addresidentl" class="btn btn-success" style="margin-top: 24px;">+</button>
                                    </span>
                                    @if($cmscontentdiv3->count()>0)
                                    @foreach($cmscontentdiv3->get() as $contdiv3)
                                    <span id="createresident">
                                         <div class="form-group">
                                            <input type="file" name="image[]" id="image[]" class="form-control" accept="image/*" onchange="GetFileSize()">
                                            <small class="text-warning">Note :- Upload image with .jpg, .jpeg and .png extensions upto 1 MB.</small>
                                            <label id="image" class="image">Please select cover image</label>
                                            <label id="image_error" class="image_error">Image size is large.</label>
                                            <label id="image_type_error"  class="image_type_error">Select image only .jpg, .png, .jpeg file</label>
                                      </div>
                                        <div class="form-group">
                                        <input type="text" name="title[]" id="title[]" placeholder="Residentia Title" class="form-control character" maxlength="25" value="{{$contdiv3->title}}">
                                        @if ($errors->has('name'))
                                            <span class="text-danger" style="color: red">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                    <textarea class="form-control ckeditor" name="description[]" id="description[]" rows="5" placeholder="Description">{{ $contdiv3->description }}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('description') }}</span>
                                    @endif
                                    <!-- <button type="button" name="addresidentl" id="addresidentl" class="btn btn-success" style="margin-top: 24px;">+</button> -->
                                    </span>
                                    @endforeach
                                    @endif
                                </div>
                                
                                <div class="box-footer csm-service">
                                   <div class="btn-group">
                                     <a href="{{ route('admin.services.index') }}" class="btn btn-default">Cancel</a>
                            
                                      <button type="submit" class="btn btn-primary" id="add_submit">Save</button>
                                   </div>
                                </div>
                            </div>
                        </form>
                         <form action="{{ route('admin.cmscontent.store') }}" method="post" class="form" enctype="multipart/form-data" id="add_residential">
                            {{ csrf_field() }}
                            <div class="row">
                                <input type="hidden" name="page_id" value="1">
                                <input type="hidden" name="div_id" value="4">
                                <div class="col-lg-6">
                                    <h3 class="page-title header-color">Commercial Description</h3>
                                    <p>Elevation + wet flood proofing techniques are the suggested form of building compliance.</p>
                                </div>
                                <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="file" name="image" id="image" class="form-control" accept="image/*" onchange="GetFileSize()">
                                    <small class="text-warning">Note :- Upload image with .jpg, .jpeg and .png extensions upto 1 MB.</small>
                                    <label id="image" class="image">Please select cover image</label>
                                    <label id="image_error" class="image_error">Image size is large.</label>
                                    <label id="image_type_error"  class="image_type_error">Select image only .jpg, .png, .jpeg file</label>
                                </div>
                                    <div class="form-group">
                                      <input type="text" name="title" id="title" placeholder="Residentia Title" class="form-control character" maxlength="25">
                                    @if ($errors->has('name'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('name') }}</span>
                                    @endif
                                  </div>
                                    <textarea class="form-control ckeditor" name="description" id="description" rows="5" placeholder="Description">{{ old('description') }}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('description') }}</span>
                                    @endif
                                </div>
                                <div class="box-footer csm-service">
                                   <div class="btn-group">
                                     <a href="{{ route('admin.services.index') }}" class="btn btn-default">Cancel</a>
                            
                                      <button type="submit" class="btn btn-primary" id="add_submit">Save</button>
                                   </div>
                                </div>
                            </div>
                        </form>
                        <form action="{{ route('admin.cmscontent.store') }}" method="post" class="form" enctype="multipart/form-data" id="add_smartVentProducts">
                            {{ csrf_field() }}
                            <div class="row">
                                <input type="hidden" name="page_id" value="1">
                                <input type="hidden" name="div_id" value="5">
                                <div class="col-lg-6">
                                    <h3 class="page-title header-color">Smart Vent Products Description</h3>
                                    <p>Smart Vent Products, Inc. was born out of the need to create flood vents —a safeguard mandated by the Federal Emergency Management Agency, the National Flood Insurance Program, and State and National Construction Codes.</p>
                                </div>
                                <div class="col-lg-6">
                                    <textarea class="form-control ckeditor" name="description" id="description" rows="5" placeholder="Description">{{ old('description') }}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('description') }}</span>
                                    @endif
                                </div>
                                <div class="box-footer csm-service">
                                   <div class="btn-group">
                                     <a href="{{ route('admin.services.index') }}" class="btn btn-default">Cancel</a>
                            
                                      <button type="submit" class="btn btn-primary" id="add_submit">Save</button>
                                   </div>
                                </div>
                            </div>
                        </form>
                        <form action="{{ route('admin.cmscontent.store') }}" method="post" class="form" enctype="multipart/form-data" id="add_service">
                            {{ csrf_field() }}
                            <div class="row">
                                <input type="hidden" name="page_id" value="1">
                                <input type="hidden" name="div_id" value="6">
                                <div class="col-lg-6">
                                    <h3 class="page-title header-color">Services</h3>
                                </div>
                                 <div class="col-lg-6">
                                 <div class="form-group">
                                    <input type="file" name="image" id="image" class="form-control" accept="image/*" onchange="GetFileSize()">
                                    <small class="text-warning">Note :- Upload image with .jpg, .jpeg and .png extensions upto 1 MB.</small>
                                    <label id="image" class="image">Please select cover image</label>
                                    <label id="image_error" class="image_error">Image size is large.</label>
                                    <label id="image_type_error"  class="image_type_error">Select image only .jpg, .png, .jpeg file</label>
                                  </div>
                                  <div class="form-group">
                                     <input type="text" name="title" id="title" placeholder="Service Title" class="form-control character" maxlength="25">
                                      @if ($errors->has('name'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('name') }}</span>
                                      @endif
                                  </div>
                                 </div>
                                 <div class="box-footer csm-service">
                                   <div class="btn-group">
                                     <a href="{{ route('admin.services.index') }}" class="btn btn-default">Cancel</a>
                            
                                      <button type="submit" class="btn btn-primary" id="add_submit">Save</button>
                                   </div>
                                </div>
                            </div>
                        </form>
                        <form action="{{ route('admin.cmscontent.store') }}" method="post" class="form" enctype="multipart/form-data" id="add_smartVent">
                            {{ csrf_field() }}
                          <div class="row">
                            <input type="hidden" name="page_id" value="1">
                            <input type="hidden" name="div_id" value="7">
                            <div class="col-lg-6">
                                <h3 class="page-title header-color">LOOKING FOR SMART VENTS?</h3>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                     <input type="text" name="title" id="title" placeholder="LOOKING FOR SMART TITLE" class="form-control character" maxlength="25">
                                      @if ($errors->has('name'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('name') }}</span>
                                      @endif
                                  </div>
                            </div>
                            <div class="box-footer csm-service">
                                   <div class="btn-group">
                                     <a href="{{ route('admin.services.index') }}" class="btn btn-default">Cancel</a>
                            
                                      <button type="submit" class="btn btn-primary" id="add_submit">Save</button>
                                   </div>
                                </div>
                          </div>
                        </form>
                    <!-- /.box-body -->
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection


@section('js')

<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script>
    $(document).ready(function(){
         var i=1;
        $('#addresidentl').click(function(){
            i++;
            $("#createresident").append('<span id="createresident'+i+'"><div class="form-group"><input type="file" name="image[]" id="image[]" class="form-control" accept="image/*" onchange="GetFileSize()"><small class="text-warning">Note :- Upload image with .jpg, .jpeg and .png extensions upto 1 MB.</small><label id="image" class="image">Please select cover image</label><label id="image_error" class="image_error">Image size is large.</label><label id="image_type_error"  class="image_type_error">Select image only .jpg, .png, .jpeg file</label></div><div class="form-group"><input type="text" name="title[]" id="title[]" placeholder="Residentia Title" class="form-control character" maxlength="25"></div> <textarea class="form-control ckeditor" name="description[]" id="description[]" rows="5" placeholder="Description"></textarea><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove" style="margin-top: 24px;">x</button></span>');

            //$('.ckeditor').ckeditor();
            //$("#createresident").find('textarea').ckeditor();
           // var createevent= $("#createresident").html("remove");
            //$("#createresident").clone().html('dfd').appendTo(createevent);
        });
        $(document).on('click', '.btn_remove', function(){  
            var button_id = $(this).attr("id");   
            $('#createresident'+button_id+'').remove();  
        });
    });
</script>
<script type="text/javascript">

    @if(session()->has('message'))
      toastr.success("{{ session()->get('message') }}");
    @elseif(session()->has('error'))
      toastr.error("{{ session()->get('error') }}");
    @endif
</script>
@endsection

