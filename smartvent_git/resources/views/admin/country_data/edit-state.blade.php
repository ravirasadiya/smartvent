@extends('layouts.admin.app')
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color"> Edit State</h4>
                    </div>
                </div>
                <form action="{{ route('admin.country_data.state_store') }}" method="post" class="form" enctype="multipart/form-data" id="add_technical">
                    <div class="box-body">
                        <div class="row">
                            {{ csrf_field() }}
                            <div class="col-md-12">
                                <div class="row">
                                    <input type="hidden" name="STCode" id="id" value="@if(isset($states)) {{$states->STCode}} @endif">
                                    <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="state_id">State <span class="text-danger">*</span></label>
                                            <input type="text" name="state" id="state" placeholder="State" class="form-control"  maxlength="25" value="@if(isset($states)) {{$states->name}} @endif">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="state_id">राज्य <span class="text-danger">*</span></label>
                                            <input type="text" name="state_hi" id="state_hi" placeholder="State" class="form-control state_hi"  maxlength="25" value="@if(isset($states)) {{$states->DTName_hi}} @endif">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="state_id">રાજ્ય <span class="text-danger">*</span></label>
                                            <input type="text" name="state_gu" id="state_gu" placeholder="State" class="form-control state_gu"  maxlength="25" value="@if(isset($states)) {{$states->DTName_gu}} @endif">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            @include('admin.shared.status-select', ['status' => $states->status])
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <a href="{{ route('admin.country_data.index') }}" class="btn btn-default">Cancel</a>
                            <input type="button" class="btn btn-info" value="Translate" id="translate">
                            <button type="submit" id="add_submit" class="btn btn-primary">Update </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection


@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">
    $("#translate").on('click',function(){
            $("#add_submit").css('display','block');
            var state = $("#state").val();
            var tempArray = {};
            tempArray["state"] = state;
            $(".box").css({'opacity':'0.5','pointer-events': 'none'});
            $.ajax({
                url : "{{route('admin.translate')}}",
                data:  tempArray,
                type: 'get',
                dataType: 'json',
                success: function( result )
                {
                    var jsonObj = result;
                    $("#state_hi").val(jsonObj.state.hi);
                    $("#state_gu").val(jsonObj.state.gu);
                    for (myObj in jsonObj) {
                        var tempName = String(myObj); 
                        $("."+tempName+"_hi").val(jsonObj[myObj].hi);
                        $("."+tempName+"_gu").val(jsonObj[myObj].gu);
                    } 
                    $(".box").css({'opacity':'1','pointer-events': 'auto'});
                },
                error: function(error)
                {
                    console.log(error);
                }
            });
        });
    $("#add_technical").validate({
        ignore: [],
        errorClass: 'error',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
            name: {
                required: true,
                maxlength: 25,
            },
            state_id: {
                required: true,
            }
        },
        messages: {
            "name":{
                required: "Please enter disritct name",
                maxlength: "Please enter max 25 character",
            },
            "state_id":{
                required: "Please select state",
            }
        },
        submitHandler:  function(form) {
            $('button[type="submit"]').attr('disabled', true);
            form.submit();
        },
    });
    
    
</script>
@endsection
