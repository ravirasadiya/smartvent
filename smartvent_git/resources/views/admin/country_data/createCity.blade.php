@extends('layouts.admin.app')
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        @if(isset($tehsils))
                        <h4 class="page-title header-color"> Edit District</h4>
                        @else
                        <h4 class="page-title header-color"> Add District</h4>
                        @endif
                    </div>
                </div>
                @if(isset($tehsils))
                <form action="{{ route('admin.country_data.disritct_update') }}" method="post" class="form" enctype="multipart/form-data" id="add_technical">
                @else    
                <form action="{{ route('admin.country_data.store') }}" method="post" class="form" enctype="multipart/form-data" id="add_technical">
                @endif
                    <div class="box-body">
                        <div class="row">
                            {{ csrf_field() }}
                           
                            <input type="hidden" name="DTCode" id="DTCode" placeholder="" class="form-control"  maxlength="25" value="@if(isset($tehsils)) {{$tehsils->DTCode}} @endif">
                            <input type="hidden" name="STCode" id="STCode" placeholder="" class="form-control"  maxlength="25" value="@if(isset($tehsils)) {{$tehsils->STCode}} @endif">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="state_id">State <span class="text-danger">*</span></label>
                                            <select name="state_id" id="state_id" class="form-control">
                                                @if($list)   
                                                    <option value="">Select State</option>
                                                    @foreach($list as $state)
                                                        <option value="{{ $state->STCode }}" @if(isset($tehsils->STCode)) @if($tehsils->STCode==$state->STCode) selected @endif @endif>{{ $state->name }}</option>
                                                    @endforeach
                                                @endIf
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="name">District <span class="text-danger">*</span></label>
                                            <input type="text" name="name" id="name" placeholder="Disritct name" class="form-control"  maxlength="25" value="@if(isset($tehsils)) {{$tehsils->name}} @endif">
                                            @if ($errors->has('name'))
                                                <span class="text-danger" style="color: red">{{ $errors->first('name') }}</span>
                                            @endif
                                            <span id="uname_response"></span>
                                        </div>
                                    </div>
                                     <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="name">जिला <span class="text-danger">*</span></label>
                                            <input type="text" name="name_hi" id="name_hi" placeholder="Disritct name" class="form-control"  maxlength="25" value="@if(isset($tehsils)) {{$tehsils->DTName_hi}} @endif">
                                            @if ($errors->has('name'))
                                                <span class="text-danger" style="color: red">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                     <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="name">જીલ્લો <span class="text-danger">*</span></label>
                                            <input type="text" name="name_gu" id="name_gu" placeholder="Disritct name" class="form-control"  maxlength="25" value="@if(isset($tehsils)) {{$tehsils->DTName_gu}} @endif">
                                            @if ($errors->has('name'))
                                                <span class="text-danger" style="color: red">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="col-md-12">-->
                            <!--    <div class="row">-->
                            <!--        <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">-->
                            <!--            <div class="form-group">-->
                            <!--                @if(isset($tehsils))-->
                            <!--                    @include('admin.shared.status-select', ['status' => $tehsils->status])-->
                            <!--                @else-->
                            <!--                    @include('admin.shared.status-select', ['status' => 1])-->
                            <!--                @endif-->
                            <!--            </div>-->
                            <!--        </div>-->
                            <!--    </div>-->
                            <!--</div>-->
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <a href="{{ route('admin.country_data.index') }}" class="btn btn-default">Cancel</a>
                            <input type="button" class="btn btn-info" value="Translate" id="translate">
                            @if(isset($tehsils))
                            <button type="submit" class="btn btn-primary">Update </button>
                            @else
                            <button type="submit" class="btn btn-primary">Add </button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection


@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">
           $("#name").keyup(function(){
                  var username = $(this).val().trim();
                   var stateid = $('#state_id').val();
                   var DTCode =$('#DTCode').val();
                   console.log(DTCode);
                   
                   if(username != '' && DTCode!=''){
                       
                        $.ajax({
                                url: "{{route('admin.country_data.checkEditDistrictname')}}",
                                type: 'get',
                                data: {name: username,stateid:stateid,DTCode:DTCode},
                                success: function(response){
                                    
                                    if(response=='true'){
                                       $('#uname_response').html("<span style='color: red;'>District already exist.</span>");
                                       $("#name").focus();
                                       $('.btn-primary').attr('disabled', true);
                                       return false;
                                    }
                                     else{
                                        $("#uname_response").empty();
                                      $('.btn-primary').removeAttr('disabled');
                                    }
                    
                                 }
                             });
                   }
                   else{
                        if(username != ''){
                             $.ajax({
                                url: "{{route('admin.country_data.checkDistrictname')}}",
                                type: 'get',
                                data: {name: username,stateid:stateid},
                                success: function(response){
                                    
                                    if(response=='true'){
                                       $('#uname_response').html("<span style='color: red;'>District already exist.</span>");
                                       $("#name").focus();
                                       $('.btn-primary').attr('disabled', true);
                                       return false;
                                    }
                                     else{
                                        $("#uname_response").empty();
                                      $('.btn-primary').removeAttr('disabled');
                                    }
                    
                                 }
                             });
                      } else{
                            $("#uname_response").empty();
                            $('.btn-primary').removeAttr('disabled');
                      }
                       
                   }
                  

             });
        $("#translate").on('click',function(){
            $("#add_submit").css('display','block');
            var name = $("#name").val();
            var tempArray = {};
            tempArray["name"] = name;
            $(".box").css({'opacity':'0.5','pointer-events': 'none'});
            $.ajax({
                url : "{{route('admin.translate')}}",
                data:  tempArray,
                type: 'get',
                dataType: 'json',
                success: function( result )
                {
                    var jsonObj = result;
                    $("#name_hi").val(jsonObj.name.hi);
                    $("#name_gu").val(jsonObj.name.gu);
                    for (myObj in jsonObj) {
                        var tempName = String(myObj); 
                        $("."+tempName+"_hi").val(jsonObj[myObj].hi);
                        $("."+tempName+"_gu").val(jsonObj[myObj].gu);
                    } 
                    $(".box").css({'opacity':'1','pointer-events': 'auto'});
                },
                error: function(error)
                {
                    console.log(error);
                }
            });
        });
    $("#add_technical").validate({
        ignore: [],
        errorClass: 'error',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
            name: {
                required: true,
                maxlength: 25,
            },
            state_id: {
                required: true,
            }
        },
        messages: {
            "name":{
                required: "Please enter disritct name",
                maxlength: "Please enter max 25 character",
            },
            "state_id":{
                required: "Please select state",
            }
        },
        submitHandler:  function(form) {
            $('button[type="submit"]').attr('disabled', true);
            form.submit();
        },
    });
    
    
</script>
@endsection
