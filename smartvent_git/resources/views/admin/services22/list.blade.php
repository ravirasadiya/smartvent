@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if($services)
            <div class="box">
                <div class="box-body">
                    <div class="box-body">
                        <div class="box-header with-border bg-title border-header">
                            <h4 class="page-title header-color">Manage Services</h4>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table border-b1px" id="table-id">
                            <thead>
                                <tr>
                                    <th class="col-md-3">Page</th>
                                    <th>Images</th>
                                    <th>Title</th>
                                    <th>Status</th>
                                    <th class="col-md-3">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($services as $service)
                                <tr>
                                    <td>{{ hidden }}</td>
                                    <td>
                                        @if(isset($service->image))
                                            <img  style="height:100px !important;width:100px !important" src="{{$service->image}}" alt="silders image" class="img-thumbnail">
                                        @endif
                                    </td>
                                    
                                    <td>
                                        @include('layouts.status', ['status' => $service->status])
                                    </td>
                                    <td>
                                        <input type="hidden" name="_method" value="delete">
                                        <div class="btn-group">      
                                                   
                                            @if($permission->edit==1)
                                                <a href="{{ route('admin.services.edit', $service['id']) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                            @endif
                                            @if($permission->delete==1)
                                                <a href="{{ route('admin.services.destroy', $service['id']) }}" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm" ><i class="fa fa-times"></i></a>
                                                   
                                            @endif
                                        </div>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>    
                    </div>
                </div>
                <div class="box-footer">
                    <div class="btn-group">
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        @else
            <div class="box">
                <div class="box-body">
                    <p class="alert alert-warning">No silders found.</p>
                </div>
                <div class="box-footer">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
        @endif

    </section>
    <!-- /.content -->
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('#table-id').DataTable();
    } );
</script>
@endsection
