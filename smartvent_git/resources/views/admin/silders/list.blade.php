@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if($silders)
            <div class="box">
                <div class="box-body">
                    <div class="box-body">
                        <div class="box-header with-border bg-title border-header">
                            <h4 class="page-title header-color">Manage Silders</h4>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table border-b1px" id="table-id">
                            <thead>
                                <tr>
                                    <th class="col-md-3">Silder Image</th>
                                    <th class="col-md-3">Link</th>
                                    <th class="col-md-3">Title</th>
                                    <th>Status</th>
                                    <th class="col-md-3">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($silders as $silder)
                                <tr>
                                    <td>
                                        @if(isset($silder->image))
                                            <img  style="height:100px !important;width:100px !important" src="{{$silder->image}}" alt="silders image" class="img-thumbnail">
                                        @endif
                                    </td>
                                    <td>
                                        {{ $silder->link }}
                                        
                                    </td>
                                    <td>
                                        {{ $silder->title }}
                                        
                                    </td>
                                    <td>
                                        @include('layouts.status', ['status' => $silder->status])
                                    </td>
                                    <td>
                                        <form action="" method="post" class="form-horizontal">
                                            
                                            <input type="hidden" name="_method" value="delete">
                                            <div class="btn-group">

                                                    <a href="" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                               
                                                    <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> </button>
                                             
                                            </div>
                                        </form> 
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>    
                    </div>
                </div>
                <div class="box-footer">
                    <div class="btn-group">
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        @else
            <div class="box">
                <div class="box-body">
                    <p class="alert alert-warning">No silders found.</p>
                </div>
                <div class="box-footer">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
        @endif

    </section>
    <!-- /.content -->
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('#table-id').DataTable();
    } );
</script>
@endsection
