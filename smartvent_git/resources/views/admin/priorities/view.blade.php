@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">Edit Priorities</h4>
                    </div>
                </div>
                <form action="{{ route('admin.pro_priority.update', $priority->id) }}" id="edit_permission" method="post" class="form"> 
             <?php /*?>   <form id="edit_permission" method="post" class="form"><?php  */?> 
                    <div class="box-body">
                        {{ csrf_field() }}
                        <input type="hidden" value="put" name="_method">
                        <!--<input type="hidden" value="1" name="view_priority">-->
                        
                        <!-- <div class="box"> -->
                            <div class="box-body">
                                <h4>Products</h4>
                                
                                    <table class="table table-striped allproductsearch" id="data">
                                        <thead>
                                          <tr>
                                              <th>No111</th>
                                               <th>Company</th>
                                              <th>Technical</th>
                                              <th>Product Name</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i=1; ?>
                                        @if($products)
                                            @foreach($products as $product)
                                                <tr class="sortable" id="{{ $product->id }}">
                                                    <td>{{ $i++ }} 
                                                    <input type="hidden" name="productid[]" value="{{ $product->id }}"/>
                                                    </td>
                                                     <td>{{ $product->company_name }}</td>
                                                    <td>{{ $product->technical_name }}</td>
                                                    <td>{{ $product->name }}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    <!-- </div> -->
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <div class="btn-group">
                                <a href="{{ route('admin.pro_priority.index') }}" class="btn btn-default">Cancel</a>
                                <!--<button type="submit" id="update" class="btn btn-primary">Next</button>-->
                                <button type="submit" id="update" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection

@section('js')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
        $('.allproductsearch').DataTable({
             "info": false,
             "paging": false,
             "ordering":false
        });
    });

    sort_order=[];
    newabc = [];
     var ordertxt= new Array();
    $(document).ready(function() {
      $("table tbody").sortable({ 
            update: function(event, ui) {  
                $('table tbody tr').each(function() {
                   
                    $(this).children('td:first-child').html($(this).index()+1);
                });
                // console.log(sort_order);
             /*    var ordertxt= new Array();
$("input[name='productid[]']").each(function(){
    ordertxt.push($(this).val());
});  
 console.log(ordertxt);
            */
            },
            stop: function(event, ui) {
                $(this).find('.sortable').each(function(i, el){
                    //console.log('update122');
                    var i_new = (i+1);
                    sort_order[i_new] = $(this).attr('id');
                    newabc.push($(this).attr('id'));
                });
                console.log(sort_order);
                /*   var ordertxt= new Array();
$("input[name='productid[]']").each(function(){
    ordertxt.push($(this).val());
});  
 console.log(ordertxt);*/
            }
        }).disableSelection();   
    });
    $('#subcategory_id').change(function(){
        $("#data option").remove();
        var id = $(this).val();
        var category_id = $('#category_id').val();
        $.ajax({
            url : "{{ route('admin.priority.getProduct') }}",
            data: {
                "_token": "{{ csrf_token() }}",
                "id": id,
                "category_id": category_id
                },
            type: 'post',
            dataType: 'json',
            success: function( result )
            {
                console.log(result);
                var table = $('#data');
                table.find("tbody tr").remove();
                var i=1;
                $.each( result, function(k, v) {
                    table.append("<tr class='sortable' id="+k+"><td><input type='checkbox' value="+i+"></td><td>" + v + "</td></tr>");
                    // table.append("<tr class='sortable' id="+k+"><td><input type='checkbox' value="+i+">" + + "</td><td>" + v + "</td></tr>");
                    // sort_order[i]=k;
                    // i=i+1;
                });
            },
            error: function()
            {
                alert('error...');
            }
        });
    });

     $('#category_id').change(function(){
        $("#subcategory_id option").remove();
        var id = $(this).val();
        $.ajax({
            url : "{{route('admin.products.getSubCategories')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "id": id
                },
            type: 'post',
            dataType: 'json',
            success: function( result )
            {
                console.log(result);
                $('#subcategory_id').append($('<option>', {value:'', text:'Select'}));
                $.each( result, function(k, v) {
                    $('#subcategory_id').append($('<option>', {value:k, text:v}));
                });
            },
            error: function()
            {
                alert('error...');
            }
        });
        
        
        $("#data option").remove();
        var id = $(this).val();
        if(id == "all"){
            $.ajax({
                url : "{{ route('admin.priority.getAllProduct') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                    },
                type: 'post',
                dataType: 'json',
                success: function( result )
                {
                    console.log(result);
                    var table = $('#data');
                    table.find("tbody tr").remove();
                    var i=1;
                    $.each( result, function(k, v) {
                        table.append("<tr class='sortable' id="+k+"><td><input type='checkbox' value="+i+"></td><td>" + v + "</td></tr>");
                        // table.append("<tr class='sortable' id="+k+"><td>" + i + "</td><td>" + v + "</td></tr>");
                        // sort_order[i]=k;
                        // i=i+1;
                    });
                },
                error: function()
                {
                    alert('error...');
                }
            });
        }else{
            $.ajax({
                url : "{{ route('admin.priority.getCategoryProduct') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                    },
                type: 'post',
                dataType: 'json',
                success: function( result )
                {
                    console.log(result);
                    var table = $('#data');
                    table.find("tbody tr").remove();
                    var i=1;
                    $.each( result, function(k, v) {
                        table.append("<tr class='sortable' id="+k+"><td><input type='checkbox' value="+i+"></td><td>" + v + "</td></tr>");
                        // table.append("<tr class='sortable' id="+k+"><td>" + i + "</td><td>" + v + "</td></tr>");
                        // sort_order[i]=k;
                        // i=i+1;
                    });
                },
                error: function()
                {
                    alert('error...');
                }
            });
        }
    });

    $('#update').click(function(){
       // alert('er');
       
       console.log(sort_order.length);
      //  var ordertxt= new Array();
$("input[name='productid[]']").each(function(){
    ordertxt.push($(this).val());
});  
if(sort_order.length>0){
     $.ajax({
           type: 'POST',
           url: "{{ route('admin.priority.subcat_sort') }}",
           data: {id:sort_order,'_token':"{{ csrf_token() }}"},
           success: function(data){
           console.log(data);
          // return false;
           }, error : function(error){
                alert("Something went wrong please try again.");
           }
        });
      //  return false;
}
       else{
           $.ajax({
           type: 'POST',
           url: "{{ route('admin.priority.subcat_sort') }}",
           data: {id:ordertxt,'_token':"{{ csrf_token() }}"},
           success: function(data){
           console.log(data);
           //return false;
           }, error : function(error){
                alert("Something went wrong please try again.");
           }
        });
        //return false;
       }
    });
</script>

@endsection

