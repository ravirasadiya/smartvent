<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Order Invoice</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <style type="text/css">
        table { border-collapse: collapse;}
    </style>
</head>
<body>
    <section class="row">
      
        <div style="margin-top:3%; text-align: center">
            <img class="logofull" src="{{ 'http://khedutbolo.com/New Project.jpg' }}" width="auto" height="50px">
        </div>
        
    </section>
    <section class="row">
        <div style="margin-left: 60%; margin-top: 2%">
            Order ID: {{$order['id']}}<br/>
            Invoice No: GT/000{{$order['id']}} <br />
            Invoice to: {{$customer->name}} <br />
            Mobile No.{{$address->phone}} <br />
            Deliver to: <strong>{{ $address->alias }} <br /></strong>
            Address: {{ $address->address_1 }} <br />
            Village: {{ $address->village_id }} {{ $address->tehsil_id }} <br />
            City: {{ $address->city }}<br/> 
            State: {{ $address->state_code }} <br />
            Country:{{ $address->country_id }} {{ $address->zip }}

        </div>
        <div class="pull-right">
            From: {{config('app.name')}}<br>
        </div>
    </section>
    <section class="row">
        <div class="col-md-12">
            <h3 style="text-align: center;">Retail/Tax Invoice</h3>
            <table class="table table-striped" width="100%" border="0" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                         
                        <th style="width: 14%; border: 1px solid black; border-collapse: collapse;text-align: center;">Name</th>
                        @if($offer_status==1)
                            <th style="width: 18%; border: 1px solid black; border-collapse: collapse;text-align: center;">Offer Product</th>
                        @endif
                         <th style="width: 14%; border: 1px solid black; border-collapse: collapse;text-align: center;">HSN Code</th>
                        <th style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: center;">Unit</th>
                      
                        <th style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: center;">Quantity</th>
                        <th style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: center;">Price</th>
                         <?php /* ?>
                        <th style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;">Shipping Price</th>
                        <?php */?>
                        <th style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: center;">GST %</th>
                        <th style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: center;">CGST</th>
                        <th style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: center;">SGST</th>
                        <th style="width: 14%; border: 1px solid black; border-collapse: collapse;text-align: center;">Total</th>
                        
                    </tr>
                </thead>
                <tbody>
                @php
                    $tq=0;
                    $discounts = 0;
                    $finalnewgst = 0;
                @endphp
                <?php  //$total = ($product_price*$product->quantity)+$product->shipping_price;?>
                @foreach($products as $product)
                    @php 
                        $tq=$tq+$product->quantity; 
                        $product_price = str_replace(",","",$product->product_price);
                        $shipping_price = number_format($product->shipping_price, 2);
                        $gst = ($product_price*$product->quantity*$product->gst_price)/100;
                        
                      
                        
                       
                        $total = ($product_price*$product->quantity);
                        
                        $newgst= ($product_price*$gst)/100;
                        
                        $total = number_format($total, 2);
                        
                        $finaltotal=($product_price)+($newgst);
                        $finalgst = $newgst/2;
                        $finalnewgst+=$finalgst;
                        $discount[] = $product->discount;
                    @endphp
                    <tr>
                        
                        <td style="width: 18%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">{{$product->product_name}} </td>
                          @if($offer_status==1)
                            <td style="width: 18%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">{{$product->offer_product}}</td>
                        @endif
                         <td style="width: 14%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">{{$product->hsn_code}}</td>
                        <td style="width: 10%; border: 1px solid black; border-collapse: collapse; margin-left:10px;text-align: center;">{{$product->unit}}</td>
                       
                        <td style="width: 12%; border: 1px solid black; border-collapse: collapse;text-align: center;">{{$product->quantity}}</td>
                        
                             
                              <td style="width: 12%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{number_format($product_price - $gst, 2)}}</td>
                            <?php /* ?>
                             <td style="width: 12%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">
                            @if($attr!='' || $attr!=null)
                                {{$attr->sale_price}}
                             
                             @else   
                                 {{$product_price}}
                             @endif
                             </td>
                       
                        <td style="width: 12%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{$product_price - $gst}}</td>
                         
                       
                       
                         <td style="width: 18%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{$shipping_price}}</td>
                          <?php */ ?>
                        <td style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{$product->gst_price}}%</td>
                          <td style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{ number_format(($gst/2),2) }}</td>
                            <td style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{ number_format(($gst/2),2) }}</td>
                           <td style="width: 14%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{ $total}}</td>
                           
                             <?php /*?>  <td style="width: 14%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">
                                   @if($attr!='' || $attr!=null)
                                {{$attr->sale_price}}
                             
                             @else   
                                 {{$product_price}}
                             @endif
                                  </td>
                          
                           <td style="width: 14%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{$total}}</td>
                       <?php /*?>
                        <td style="width: 14%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{$total}}</td>
                        <?php */?>
                      
                    </tr>
                @endforeach
                
                </tbody>
            </table>
        </div>

        <div class="box">
            <div class="box-body">
                <h3 style="text-align: center;">Order Information</h3>
                <table class="table  table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;"><strong>Date</strong></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;"><strong>Customer</strong></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;"><strong>Quantity</strong></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;"><strong>Payment</strong></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;"><strong>Status</strong></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;">{{ date('M d, Y', strtotime($order['created_at'])) }}</td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;">{{ $customer->name }}</td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;">{{ $tq }}</td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse; margin-left:10px;"><strong>{{ $order['payment'] }}</strong></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;"">{{ucfirst($statuses)}}</td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">Sub Total:</td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{number_format(($order->total_products) - ($order->tax),2)}}</td>
                        <?php /*?>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{$order->total_products - $order->tax}}</td>
                        <?php */?>
                    </tr>
                    <?php /*?>
                    @if($order['wallet_point']!=0.0 && $order['wallet_point']!="")
                        @php
                            $wallet_amount = $order['wallet_point'] * env('DISCOUNT_POINT');
                            $wallet_amount = $wallet_amount + array_sum($discount);
                            $wallet_amount = number_format($wallet_amount, 2);
                        @endphp
                        <tr>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">- Discounts:</td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{$wallet_amount}}</td>
                        </tr>
                    @else
                        <tr>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">- Discounts:</td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{number_format(array_sum($discount),2)}}</td>
                        </tr>
                    @endif
                     <?php */?>
                    <tr>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">+ Tax:</td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{$order->tax}}</td>
                    </tr>
                    <tr>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">+ Shipping Price:</td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{$order->total_shipping}}</td>
                    </tr>
                    <?php /*?>
                    @php 
                        $wallet_amount = $order['wallet_point'] * env('DISCOUNT_POINT');
                        if (isset($wallet_amount)) {
                            $total1 = $order->total + $order->total_shipping - $wallet_amount - array_sum($discount);
                            $total1 = number_format($total1, 2);
                        } else{
                            $total1 = $order->total + $order->total_shipping - array_sum($discount) ;
                            $total1 = number_format($total1, 2);
                        }
                    @endphp
                     <?php */?>
                      @php 
                        $wallet_amount = $order['wallet_point'] * env('DISCOUNT_POINT');
                        if (isset($wallet_amount)) {
                            $total1 = $order->total + $order->total_shipping - $wallet_amount;
                            $total1 = number_format($total1, 2);
                        } else{
                            $total1 = $order->total + $order->total_shipping;
                            $total1 = number_format($total1, 2);
                        }
                    @endphp
                    <tr>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse; margin-left:10px;"><strong>Total:</strong></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;"><strong>Rs {{ $total1 }}</strong></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </section>
    <section class="row">
        <div style="margin-top:100px; text-align: center">
            <img class="logofull" src="{{ 'http://khedutbolo.com/barcodenew.jpg' }}" width="auto" height="200px">
        </div>
    </section>
</body>
</html>
                    