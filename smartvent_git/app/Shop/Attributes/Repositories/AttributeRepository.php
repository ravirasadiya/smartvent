<?php

namespace App\Shop\Attributes\Repositories;

use App\Shop\Attributes\Attribute;
use App\Shop\Attributes\Exceptions\AttributeNotFoundException;
use App\Shop\Attributes\Exceptions\CreateAttributeErrorException;
use App\Shop\Attributes\Exceptions\UpdateAttributeErrorException;
use App\Shop\AttributeValues\AttributeValue;
use Jsdecena\Baserepo\BaseRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Http\UploadedFile;

class AttributeRepository extends BaseRepository implements AttributeRepositoryInterface
{
    /**
     * @var Attribute
     */
    protected $model;

    /**
     * AttributeRepository constructor.
     * @param Attribute $attribute
     */
    public function __construct(Attribute $attribute)
    {
        parent::__construct($attribute);
        $this->model = $attribute;
    }

    /**
     * @param array $data
     * @return Attribute
     * @throws CreateAttributeErrorException
     */
    public function createAttribute(array $params) : Attribute
    {
        try {
            $collection = collect($params);
            if (isset($params['name'])) {
                $data['name'] = str_slug($params['name']);
            }
            
            if ($collection['image'] && $collection['image'] instanceof UploadedFile) {
                $data['image']= $collection['image']->store('crop', ['disk' => 'public']);
            }
            $crop = new Attribute($data);
           
            if (isset($params['crop_category'])) {
                $parent = $this->findAttributeById($params['crop_category']);
                $crop['crop_category'] = $parent->id;
            }

            $crop->save();
            return $crop;
        } catch (QueryException $e) {
            throw new CreateAttributeErrorException($e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return Attribute
     * @throws AttributeNotFoundException
     */
    public function findAttributeById(int $id) : Attribute
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new AttributeNotFoundException($e);
        }
    }

    /**
     * @param array $data
     * @return bool
     * @throws UpdateAttributeErrorException
     */
    public function updateAttribute(array $params) : bool
    {
        $crop = $this->findAttributeById($this->model->id);
        $collection = collect($params)->except('_token');
        $slug = str_slug($collection->get('name'));

        if (isset($params['image']) && ($params['image'] instanceof UploadedFile)) {
            $cover = $this->uploadOne($params['image'], 'crop');
            $path = $params['cover']->store('uploads');
            $merge = $collection->merge(compact('slug', 'image'));
        }else{
            $merge = $collection->merge(compact('slug')); 
        }
        if (isset($params['crop_category'])) {
            $parent = $this->findAttributeById($params['crop_category']);
            $crop->parent()->associate($parent);
        }

        $crop->update($merge->all());
        return $crop;
    }

    /**
     * @return bool|null
     */
    public function deleteAttribute() : ?bool
    {
        return $this->model->delete();
    }

    /**
     * @param array $columns
     * @param string $orderBy
     * @param string $sortBy
     * @return Collection
     */
    public function listAttributes($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection
    {
        return $this->all($columns, $orderBy, $sortBy);
    }

    /**
     * @return Collection
     */
    public function listAttributeValues() : Collection
    {
        return $this->model->values()->get();
    }

    /**
     * @param AttributeValue $attributeValue
     * @return AttributeValue
     */
    public function associateAttributeValue(AttributeValue $attributeValue) : AttributeValue
    {
        return $this->model->values()->save($attributeValue);
    }
}
