<?php

namespace App\Shop\Silders;

use Illuminate\Database\Eloquent\Model;

class HomeSilder extends Model
{
    public $table = "home_silder";
    protected $fillable = [
        'image',
        'user_id',
        'user_type'
    ];
    public $timestamps = false;
}
 