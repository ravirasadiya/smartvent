<?php

namespace App\Shop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Shop\RoleUsers\RoleUser;

class User extends Authenticatable
{

	public $table = "users";
    public $timestamps = false;

    public function hasRole()
    {
        return $this->hasMany(RoleUser::class);
    }
    public function hasPermission()
    {
        return $this->hasMany(RoleUser::class);
    }
    
}
