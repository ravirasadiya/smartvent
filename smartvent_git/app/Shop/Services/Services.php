<?php

namespace App\Shop\Services;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    public $table = "services";
    protected $fillable = [
        'image',
        'description'
    ];
    
}
