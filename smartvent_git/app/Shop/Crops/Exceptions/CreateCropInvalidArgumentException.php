<?php

namespace App\Shop\Crops\Exceptions;

class CreateCropInvalidArgumentException extends \Exception
{
}
