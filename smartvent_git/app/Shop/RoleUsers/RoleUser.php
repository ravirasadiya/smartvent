<?php

namespace App\Shop\RoleUsers;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
	public $table = "role_user";
    protected $fillable = [
        'role_id',
        'link',
        'title',
        'page',
        'plant_content'
    ];
    public $timestamps = false;

}
