<?php

namespace App\Shop\CMS;

use Illuminate\Database\Eloquent\Model;

class Cmscontent extends Model
{
     public $table = "cms_content";
    protected $fillable = [
        'page_id',
        'div_id',
        'html',
        'image',
        'status'
    ];
}
