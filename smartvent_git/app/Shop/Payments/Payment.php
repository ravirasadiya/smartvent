<?php

namespace App\Shop\Payments;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public $table = "payment";
    public $timestamps = false;
    protected $fillable = [
        'device',
        'amount',
        'transaction_id',
        'payment_type',
        'userid',
        'status'
    ];
}
