<?php

namespace App\Http\Controllers\Front;

use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use Log;
use Redirect,Response;
use Mail;
use Hash;
use App\Shop\Silders\HomeSilder;
use App\Shop\CMS\Cmscontent;
class HomeController 
{
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepo;

    /**
     * HomeController constructor.
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepo = $categoryRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data['silders'] =  HomeSilder::select('image','link','title')->where('status',1)->first();
        $data['cmscontentdiv1'] = Cmscontent::select('html')->where('div_id',1)->first();
        $data['cmscontentdiv2'] = Cmscontent::select('html','title')->where('div_id',2)->first();
        $data['cmscontentdiv3'] = Cmscontent::select('html','title','image')->where('div_id',3)->first();
        $data['cmscontentdiv4'] = Cmscontent::select('html','title','image')->where('div_id',4)->first();
         $data['cmscontentdiv5'] = Cmscontent::select('html')->where('div_id',5)->first();
          $data['cmscontentdiv6']= Cmscontent::select('image','title')->where('div_id',6)->limit(6)->get();
         $data['cmscontentdiv7'] = Cmscontent::select('html')->where('div_id',7)->first();
         
        return view('front.index')->with($data);
    }
    
    public static function contactFormSubmit(Request $request){
        try {
            $data = array(
            'fullname' => $request['firstName'].''.$request['lastName'],
            'mobile' => $request['mobile'],
            'email' => $request['email'],
            'message' => $request['message'],
            );
            if($data == '') {
                return "false";
            }else{
                
                $data['mailsend'] = Mail::send('layouts.email.email',['data'=>$data] , function($message) {
     
                    $message->to('shineinfosoft33@gmail.com', 'Khedutbolo')
                        ->from('testerdeveloper195@gmail.com','KhedutBolo')
                        ->subject('KhedutBolo Contact Detail');
                });
             
                $data['mailsend'] = Mail::send('layouts.email.emailuserthanks',['data'=>$data], function($msg) use ($data){
                        $msg->to($data['email'],$data['fullname'])
                        ->from('testerdeveloper195@gmail.com','Khedutbolo')
                        ->subject('Khedutbolo');
                    });
                $request->session()->flash('msg', 'Thank you for contact and we will get back to you within 24 hours.');
                return redirect('/');
                exit;
            }
        } catch (Exception $e) {
            Log::error($e);
        }
    }
 
    // public function update()
    // {
    //     $user = \App\Shop\User::where('id', 1)->firstOrFail();
    //     $user->password = Hash::make('12345678');
    //     $user->save();echo 'ok';exit;
    // }
}
