<?php

namespace App\Http\Controllers\Admin\Silders;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\Permission;
use App\Shop\Silders\HomeSilder;

class HomeSilderController extends Controller
{

    public function index()
    {
     $list = HomeSilder::Select('image','link','title','page','plant_content','status')->get();
     return view('admin.silders.list', [
                'silders' => $list,
                
            ]);
       // $permission = Permission::permission('silder');
        // if($permission->view==1) {
        // 	$list = HomeSilder::Select('image','link','title','page','plant_content')->get();
            
        //     return view('admin.silders.list', [
        //         'silders' => $list,
                
        //     ]);
        // } else {
        //     return view('layouts.errors.403');
        // }
        
    }

 public function create()
    {
    	return view('admin.silders.create');
        //$permission = Permission::permission('silder');
        // if($permission->add==1) {
        //     return view('admin.silders.create');
        // } else {
        //     return view('layouts.errors.403');
        // }
    }

    public function store(Request $request)
    {
    
    	$silders = new HomeSilder;
    	$silders->link = $request->link;
    	$silders->title = $request->title;
    	$silders->page = $request->page;
    	$silders->plant_content = $request->plant_content;
    	$silders->status = $request->status;
    	$url = env('APP_URL');
        if ($request->has('image')) {
            $file    = $request->image;
            $time    = md5(time());
            $profile = $file->getClientOriginalExtension();
            $name = $file->getClientOriginalName();
            $path    = "storage/silders";
            $file->move(public_path(env('IMAGE_URL').$path), $name);
            
            $silders->image = $url."/storage/silders" . "/" .$name;
        }
        $silders->save();
        $request->session()->flash('message', 'Silder created successfully');
        return redirect()->route('admin.silders.index');
        echo '<pre>';print_r($request->all());exit;
        // try{
        //     $permission = Permission::permission('category');
        //     if($permission->add==1) {
        //         $rules = [
        //             'name' => 'required',
        //             // 'company_id' => 'required',
        //             'cover' => 'required',
        //             'status'  => 'required',
        //         ];
        //         $customeMessage = [
        //             'name.required' => 'Please enter name.',
        //             'company_id.required' => 'Please  select company',
        //             'cover.required' => 'Please select cover image',
        //             'status.required' => 'Please select status',
        //         ];
        //         $validator = Validator::make($request->all(),$rules, $customeMessage);

        //         if( $validator->fails() ) {
        //              return back()->withInput()->withErrors($validator->errors());
        //         } else {
        //             $category = $this->categoryRepo->createCategory($request->except('_token', '_method'));
        //             $url = env('APP_URL');
        //             if ($request->has('image')) {
        //                 $file    = $request->image;
        //                 $time    = md5(time());
        //                 $profile = $file->getClientOriginalExtension();
        //                 $name = $file->getClientOriginalName();
        //                 $path    = "storage/silders";
        //                 $file->move(public_path(env('IMAGE_URL').$path), $name);
        //                 $profile_update          = HomeSilder::find($category->id);
        //                 $profile_update->image = $url."/storage/silders" . "/" .$name;
        //                 $profile_update->save();
        //             }
        //             if($request->parent) {
        //                 $request->session()->flash('message', 'Sub-category created successfully');
        //                 return redirect()->route('admin.categories.show', $request->parent);
        //             } else {
        //                 $request->session()->flash('message', 'Category created successfully');
        //                 return redirect()->route('admin.categories.index');
        //             }
        //         }
        //     } else {
        //     return view('layouts.errors.403');
        // }
        // } catch (Exception $e) {
        //     log::error($e);
        // }
        
    }
}
