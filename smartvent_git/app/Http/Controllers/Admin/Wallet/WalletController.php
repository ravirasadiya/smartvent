<?php

namespace App\Http\Controllers\Admin\Wallet;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Customers\Customer;
use App\Shop\Customers\Wallet;
use App\Helper\Permission;

class WalletController extends Controller
{
    public function index(){
    	try {
    	    $permission = Permission::permission('wallet');
    	    if($permission->view==1) {
        		$data['customers'] = Customer::get();
        		return view('admin.wallet.index')->with($data);
    	    } else {
                return view('layouts.errors.403');
            }
    	} catch (Exception $e) {
    		Log::error($e);
    	}
    }
    public function walletSummery($id){
    	try {
    	    $permission = Permission::permission('wallet');
    	    if($permission->view==1) {
        		$data['customer_points'] = Wallet::select('wallet.*','customers.name as customers_name')
        							->leftjoin('customers','wallet.customer_id','customers.id')
        							->where('wallet.customer_id',$id)
        							->get();
    			$data['customer'] = Customer::where('id',$id)->first();
    
                $customer_points = Wallet::where('customer_id',$id)->get();
                $totalcreditamount = array();
                $totalcreditpoint = array();
                $totaldebitamount = array();
                $totaldebitpoint = array();
                foreach ($customer_points as $customer_point) {
                    if ($customer_point->credit_or_debit == "credit") {
                        $customer_point['title'] = "Added to wallet";
                        array_push($totalcreditamount, $customer_point->amount);
                        array_push($totalcreditpoint, $customer_point->points);
                    } else{
                        array_push($totaldebitamount, $customer_point->amount);
                        array_push($totaldebitpoint, $customer_point->points);
                        $customer_point['title'] = "Paid from wallet";
                    }
                }
    
                $totalpoint = array_sum($totalcreditpoint) - array_sum($totaldebitpoint);
                $totalamount = array_sum($totalcreditamount) - array_sum($totaldebitamount);
                $totalamount = number_format($totalamount, 2);
                $data['total_point'] = $totalpoint;
                $data['totalamount'] = $totalamount;
    		return view('admin.wallet.show')->with($data);
    	    } else {
                return view('layouts.errors.403');
            }
    	} catch (Exception $e) {
    		Log::error($e);	
    	}
    }
}
