<?php

namespace App\Http\Controllers\Admin\CountriesData;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\CountriesData\CountryData;
use App\Shop\Customers\Customer;
use App\Shop\Addresses\Address;
use App\Helper\Permission;
use Stichoza\GoogleTranslate\GoogleTranslate;

class CountryDataController extends Controller
{
    public function index()
    {
        $last_data=0;
        $permission = Permission::permission('offer');
        if($permission->view==1) {
            $list = CountryData::select('STCode', 'DTName as name','status')->where('DTCode',000)->get();
            return view('admin.country_data.list', [
                'list' => $list,
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }
    
    public function create(){
        $permission = Permission::permission('country_data');
        if($permission->add==1) {
            $states = CountryData::select('STCode', 'DTName as name')->where('DTCode',000)->get();
            return view('admin.country_data.add', [
                'list' => $states,
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }
    public function edit($state_id){
        $permission = Permission::permission('country_data');
        if($permission->add==1) {
            $data['states'] = CountryData::select('STCode', 'DTName as name','DTName_gu','DTName_hi','status','STCode')->where('DTCode',000)->where('STCode',$state_id)->first();
            return view('admin.country_data.edit-state')->with($data);
        } else {
            return view('layouts.errors.403');
        }
    }
    public function stateStore(Request $request){
        try{
            $status = CountryData::where('DTCode',000)->where('STCode',$request->STCode)->first();
            $status->DTName = $request->state;
            $status->DTName_gu = $request->state_gu;
            $status->DTName_hi = $request->state_hi;
            $status->status = $request->status;
            $status->save();
            // return redirect()->route('admin.country_data.index')->with('message', 'State update successfully');
           // return redirect()->back()->with('message', 'State update successfully');
             return redirect('/admin/country_data/')->with('message', 'State update successfully');
            
        } catch (Exception $e) {
            Log::errors($e);
        }
    }
    
    public function store(Request $request){
        $state_id = $request->state_id;
        $name = $request->name;
        $data = new CountryData();
        if(isset($request->city_id)){
            $city_id = $request->city_id;
            $data->STCode = $state_id;
            $data->DTCode = $city_id;
            $getData = CountryData::where('STCode', $state_id)->where('DTCode', $city_id)->first();
            $data->DTName = $getData->DTName;
            $data->DTName_hi = $getData->name_hi;
            $data->DTName_gu = $getData->name_gu;
            if(isset($request->tehsil_id)){
                
                $last = CountryData::orderBy('TVCode','DESC')->first();
                $data->SDTCode = $request->tehsil_id;
                $data->SDTName = $getData->SDTName;
                $TVCode = $last->TVCode+1;
                $data->TVCode = $TVCode;
                $data->Name = $name;
                if($data->save()){
                    return redirect()->route('admin.country_data.index')->with('message', 'Village Added Successfully');
                }
            } else {
               
                $last = CountryData::orderBy('SDTCode','DESC')->first();
                $data->SDTName = $name;
                $SDTCode = $last->SDTCode+1;
                $data->SDTCode = $SDTCode;
                $data->TVCode = 000000;
                $data->Name = $name;
                if($data->save()){
                    return redirect()->route('admin.country_data.index')->with('message', 'Tehsil Added Successfully');
                }
            }
        }else {
           
            $last = CountryData::orderBy('DTCode','DESC')->first();
            $data->STCode = $state_id;
            $DTCode = $last->DTCode+1;
            $data->DTCode = $DTCode;
            $data->DTName = $name;
            $data->SDTCode = 00000;
            $data->SDTName = $name;
            $data->TVCode = 000000;
            $data->Name = $name;
            if($data->save()){
                return redirect()->route('admin.country_data.index')->with('message', 'District Added Successfully');
            }
        }
        
    }
    
    public function createVillage(){
        $permission = Permission::permission('country_data');
        if($permission->add==1) {
            $states = CountryData::select('STCode', 'DTName as name')->where('DTCode',000)->get();
            return view('admin.country_data.create', [
                'list' => $states,
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }
    
    public function createCity(){
        $permission = Permission::permission('country_data');
        if($permission->add==1) {
            $states = CountryData::select('STCode', 'DTName as name')->where('DTCode',000)->get();
            return view('admin.country_data.createCity', [
                'list' => $states,
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }
    
    public function show($id){
       
        $permission = Permission::permission('country_data');
        if($permission->view==1) {
           
            $cities = CountryData::select('DTCode', 'DTName as name','status')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', $id)->get();
            // foreach($cities as $city){
            //     $tehsils = countrydata::select('SDTCode', 'SDTName as name')->where('DTCode', 'like', '%' .$city->DTCode)->where('SDTCode', '!=',00000)->where('TVCode',000000)->get();
            //     // dd($tehsils);
            //     foreach($tehsils as $tehsil){
            //         $villages = countrydata::select('TVCode', 'Name')->where('SDTCode','like', '%' .$tehsil->SDTCode)->where('TVCode', '!=',000000)->get();
            //         // dd($villages);
            //         foreach($villages as $village){
            //             $vcount =countrydata::where('SDTCode','like', '%' .$tehsil->SDTCode)->where('TVCode', 'like', '%' .$village->TVCode)->count();
            //             if($vcount>1){
            //                 dd($vcount);
            //                 $vil = countrydata::where('SDTCode','like', '%' .$tehsil->SDTCode)->where('TVCode', 'like', '%' .$village->TVCode)->first();
            //                 $vil->delete();
            //             }
            //         }
            //     }
            // }
            return view('admin.country_data.show', [
                'list' => $cities,
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }
    public function showTehsil($id)
    {
        $permission = Permission::permission('country_data');
        if($permission->view==1) {
            $tehsils = countrydata::select('SDTCode', 'SDTName as name')->where('DTCode',$id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->get();
            return view('admin.country_data.showTehsil', [
                'list' => $tehsils,
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }
    public function showVillage($id)
    {
        $permission = Permission::permission('country_data');
        if($permission->view==1) {
            $villages = countrydata::select('TVCode', 'Name')->where('SDTCode',$id)->where('TVCode', '!=',000000)->get();
            foreach($villages as $village){
                $vilage =countrydata::where('SDTCode',$id)->where('TVCode', 'like', '%' .$village->TVCode)->count();
                if($vilage>1){
                    $village1s = countrydata::where('SDTCode',$id)->where('TVCode', 'like', '%' .$village->TVCode)->first();
                    $village1s->delete();
                }
            }
            // dd($villages);
            return view('admin.country_data.showVillage', [
                'list' => $villages,
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }
    public function destroy($id)
    {

        $permission = Permission::permission('offer');
        if($permission->view==1) {
            // $villages = countrydata::select('TVCode', 'Name')->where('SDTCode',$id)->where('TVCode', '!=',000000)->get();
            if(!(Customer::where('village_id',$id)->exists() || Address::where('village_id',$id)->exists())){
                $village = CountryData::where('STCode', $id);
                
              
                if($village->delete()){
                    return redirect()->back()->with('message', 'Village Deleted Successfully');
                }
            } else {
                return redirect()->route('admin.country_data.index')->with('message', 'Village can not delete because user available!!');
            }
            
        } else {
            return view('layouts.errors.403');
        }
    }
    
    public function destroydistrict($id){
         $permission = Permission::permission('offer');
        if($permission->view==1) {
            // $villages = countrydata::select('TVCode', 'Name')->where('SDTCode',$id)->where('TVCode', '!=',000000)->get();
            if(!(Customer::where('village_id',$id)->exists() || Address::where('village_id',$id)->exists())){
                $village = CountryData::where('DTCode', $id);
                
              
                if($village->delete()){
                    return redirect()->back()->with('message', 'Village Deleted Successfully');
                }
            } else {
                return redirect()->route('admin.country_data.index')->with('message', 'Village can not delete because user available!!');
            }
            
        } else {
            return view('layouts.errors.403');
        }
    }
      public function destroytehsil($id){
         $permission = Permission::permission('offer');
        if($permission->view==1) {
            // $villages = countrydata::select('TVCode', 'Name')->where('SDTCode',$id)->where('TVCode', '!=',000000)->get();
            if(!(Customer::where('village_id',$id)->exists() || Address::where('village_id',$id)->exists())){
                $village = CountryData::where('SDTCode', $id);
                
              
                if($village->delete()){
                    return redirect()->back()->with('message', 'Village Deleted Successfully');
                }
            } else {
                return redirect()->route('admin.country_data.index')->with('message', 'Village can not delete because user available!!');
            }
            
        } else {
            return view('layouts.errors.403');
        }
    }
    public function destroyvillage($id){
         $permission = Permission::permission('offer');
       
        if($permission->view==1) {
            // $villages = countrydata::select('TVCode', 'Name')->where('SDTCode',$id)->where('TVCode', '!=',000000)->get();
            if(!(Customer::where('village_id',$id)->exists() || Address::where('village_id',$id)->exists())){
                $village = CountryData::where('TVCode', $id);
                
              
                if($village->delete()){
                    return redirect()->back()->with('message', 'Village Deleted Successfully');
                }
            } else {
                return redirect()->route('admin.country_data.index')->with('message', 'Village can not delete because user available!!');
            }
            
        } else {
            return view('layouts.errors.403');
        }
    }
    
    function getCity(Request $request){
        $cities = CountryData::select('DTCode', 'DTName')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', $request->id)->get();
        $output = [];
        foreach( $cities as $city )
        {
            $output[$city->DTCode] = $city->DTName;
        }
        return $output;
    }
    
    function getTehsil(Request $request){
        $tehsils = countrydata::select('SDTCode', 'SDTName as name')->where('DTCode',$request->id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->get();
        $output = [];
        foreach( $tehsils as $tehsil)
        {
            $output[$tehsil->SDTCode] = $tehsil->name;
        }
        return $output;
    }
    
    public function DisritctEdit($id){
        try{
            $data['list'] = CountryData::select('STCode', 'DTName as name')->where('DTCode',000)->get();
            $data['tehsils'] = CountryData::select('DTCode', 'DTName as name','DTName_gu','DTName_hi','status','DTCode','STCode')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('DTCode', $id)->first();
            return view('admin.country_data.createCity')->with($data);
        }catch (Exception $e) {
            Log::errors($e);
        }
    }
    public function disritctUpdate(Request $request){
        try{
            // dd($request->all());
             //   echo $request->state_id;
               // exit;
               //echo  $request->name;
               //exit;
           // echo $request->STCode;
           //   exit;
            $district = CountryData::where('SDTCode', 00000)->where('DTCode', '!=',000)->where('DTCode', $request->DTCode)->first();
            // echo $district->DTName;
            // exit;
            $district->DTName = $request->name;
            $district->DTName_gu = $request->name_gu;
            $district->STCode = $request->state_id;
            $district->DTName_hi = $request->name_hi;
            $district->update();
            
        // return redirect()->route('admin.country_data.index')->with('message', 'District update successfully');
        return redirect('/admin/country_data/'.$request->STCode)->with('message', 'District update successfully');
          // return redirect()->action('CountryDataController@show',['id'=>$request->STCode]);
        //  return redirect()->to('/country_data/'.$request->STCode,['id'=>$request->STCode])->with('message', 'District update successfully');
          
        } catch (Exception $e) {
            Log::errors($e);
        }
    }
    public function tehsilEdit($id)
    {
        $permission = Permission::permission('country_data');
        if($permission->view==1) {
            $tehsils = countrydata::select('SDTCode', 'SDTName as name','SDTName_gu','SDTName_hi','STCode','DTCode')->where('SDTCode', '!=',00000)->where('TVCode',000000)->where('SDTCode',$id)->first();
            $states = CountryData::select('STCode', 'DTName as name')->where('DTCode',000)->get();
            return view('admin.country_data.add', [
                'tehsil' => $tehsils,
                'list' => $states,
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }
    public function tehsilStore(Request $request){
        try{
           
            $tehsils = countrydata::where('SDTCode', '!=',00000)->where('TVCode',000000)->where('SDTCode',$request->tehsil_id)->first();
            $tehsils->DTCode = $request->city_id;
            $tehsils->SDTName = $request->name;
            $tehsils->SDTName_hi = $request->name_hi;
            $tehsils->SDTName_gu = $request->name_gu;
            $tehsils->STCode = $request->state_id;
            $tehsils->save();
            
              return redirect('/admin/country_data/show-tehsil/'.$request->disritct_selected)->with('message', 'Tehsil update successfully');
            //return redirect()->back()->with('message', 'Tehsil update successfully');
        } catch (Exception $e) {
            Log::errors($e);
        }
    }
    public function villageEdit($id){
        try{
            $permission = Permission::permission('country_data');
            if($permission->view==1) {
                $villages = countrydata::select('TVCode', 'Name','Name_hi','Name_gu','STCode','DTCode','SDTCode')->where('TVCode', '!=',000000)->where('TVCode',$id)->first();
                $states = CountryData::select('STCode', 'DTName as name')->where('DTCode',000)->get();
                return view('admin.country_data.create', [
                    'list' => $states,
                    'village' => $villages,
                    'permission' => $permission
                ]);
            } else {
                return view('layouts.errors.403');
            }
        } catch (Exception $e) {
            Log::errors($e);
        }
    }
    public function checkDistrictname(Request $request)
    {
        $producntname = trim(strip_tags($request->name));
        $stateid= trim(strip_tags($request->stateid));
        $product = CountryData::where('DTName',$producntname)->where('STCode',$stateid)->count();
        if($product==0){
           echo "false";
        } else{
            echo "true";
        }
    }
     public function checkEditDistrictname(Request $request)
    {
        $producntname = trim(strip_tags($request->name));
        $stateid= trim(strip_tags($request->stateid));
        $DTCode= trim(strip_tags($request->DTCode));
       
        $product = CountryData::where('DTName',$producntname)->where('STCode',$stateid)->where('DTCode','!=',$DTCode)->count();
        if($product==0){
           echo "false";
        } else{
            echo "true";
        }
    }
    public function checkTehsilname(Request $request)
    {
        $producntname = trim(strip_tags($request->name));
        $STCode= trim(strip_tags($request->STCode));
        $DTCode = trim(strip_tags($request->DTCode));
        $product = CountryData::where('SDTName',$producntname)->where('STCode',$STCode)->where('DTCode',$DTCode)->count();
        if($product==0){
           echo "false";
        } else{
            echo "true";
        }
    }
    public function checkEditTehsilname(Request $request)
    {
        $producntname = trim(strip_tags($request->name));
        $STCode= trim(strip_tags($request->STCode));
        $DTCode = trim(strip_tags($request->DTCode));
        $SDTCode= trim(strip_tags($request->SDTCode));
        $product = CountryData::where('SDTName',$producntname)->where('STCode',$STCode)->where('DTCode',$DTCode)->where('SDTCode','!=',$SDTCode)->count();
        if($product==0){
           echo "false";
        } else{
            echo "true";
        }
    }
    public function checkVillagename(Request $request)
    {
        $producntname = trim(strip_tags($request->name));
        $STCode= trim(strip_tags($request->STCode));
        $DTCode = trim(strip_tags($request->DTCode));
        $SDTCode = trim(strip_tags($request->SDTCode));
        $product = CountryData::where('Name',$producntname)->where(['STCode'=>$STCode,'DTCode'=>$DTCode,'SDTCode'=>$SDTCode])->count();
        if($product==0){
           echo "false";
        } else{
            echo "true";
        }
    }
     public function checkEditVillagename(Request $request)
    {
        $producntname = trim(strip_tags($request->name));
        $STCode= trim(strip_tags($request->STCode));
        $DTCode = trim(strip_tags($request->DTCode));
        $SDTCode = trim(strip_tags($request->SDTCode));
        $TVCode = trim(strip_tags($request->TVCode));
       
        $product = CountryData::where('Name',$producntname)->where(['STCode'=>$STCode,'DTCode'=>$DTCode,'SDTCode'=>$SDTCode])->where('TVCode','!=',$TVCode)->count();
        if($product==0){
           echo "false";
        } else{
            echo "true";
        }
    }
    /* public function checkEditDistrictname(Request $request)
    {
        $producntname = trim(strip_tags($request->name));
        $productid=$request->productid;
        $product = CountryData::where('DTName',$producntname)->where('id','!=',$productid)->count();
        if($product==0){
          echo "false";
           //echo "<span style='color: red;'>Company already exist.</span>";
        } else{
            echo "true";
        }
    }
     public function checkVillagename(Request $request)
    {
        $producntname = trim(strip_tags($request->name));
      
        $product = CountryData::where('DTName',$producntname)->where('id','!=',$productid)->count();
        if($product==0){
          echo "false";
           //echo "<span style='color: red;'>Company already exist.</span>";
        } else{
            echo "true";
        }
    }*/
    public function villageUpdate(Request $request){
        try{
            $villages = countrydata::where('TVCode', '!=',000000)->where('TVCode',$request->village_id)->first(); 
            $villages->DTCode = $request->city_id;
            $villages->Name = $request->name;
            $villages->Name_hi = $request->name_hi;
            $villages->Name_gu = $request->name_gu;
            $villages->STCode = $request->state_id;
            $villages->SDTCode = $request->tehsil_id;
            $villages->save();
              return redirect('/admin/country_data/show-village/'.$request->tehsil_id_selected)->with('message', 'Tehsil update successfully');
           // return redirect()->route('admin.country_data.index')->with('message', 'Village update successfully');
        } catch (Exception $e) {
            Log::errors($e);
        }
    }
    
}
