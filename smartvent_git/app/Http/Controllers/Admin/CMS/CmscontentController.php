<?php

namespace App\Http\Controllers\Admin\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\Permission;
use App\Shop\CMS\Cmscontent;
class CmscontentController extends Controller
{
    public function index()
    {
       echo 'jkhdf';
    }

   /* public function create(Request $request)
    {
		$permission = Permission::permission('customer');
		$data=array();
       if($permission->add==1) {

			$data['cmscontentdiv1']=Cmscontent::where(['div_id'=>'1']);
			$data['cmscontentdiv2']=Cmscontent::where(['div_id'=>'2']);
			$data['cmscontentdiv3']=Cmscontent::where(['div_id'=>'3']);


    	return view('admin.services.create',$data);
    	} else {
            return view('layouts.errors.403');
        }
	}*/
	public function createfloodvent(Request $request)
    {
		$permission = Permission::permission('customer');
		$data=array();
       if($permission->add==1) {
			$data['cmscontentdiv1']=Cmscontent::where(['div_id'=>'1']);
			$data['cmscontentdiv2']=Cmscontent::where(['div_id'=>'2']);
			$data['cmscontentdiv3']=Cmscontent::where(['div_id'=>'3']);

			if($data['cmscontentdiv1']->count()>0){
				$data['editdescription']=$data['cmscontentdiv1']->first()->html;
			}
			else{
				$data['editdescription']="";
			}
    			return view('admin.services.createfloodevnt',$data);
    	} else {
            return view('layouts.errors.403');
        }
	}
	public function storefloodevnt(Request $request){
		try{
				//echo '<pre>';print_r($request->all());exit;
				if(Cmscontent::where(['div_id'=>1])->count()>0){
					$cmscontent= Cmscontent::where(['div_id'=>1])->first();
				}
				else{
					$cmscontent = new Cmscontent;
				}
				$cmscontent->page_id = $request->page_id;
				$cmscontent->div_id = $request->div_id;
				$cmscontent->html = $request->description;
				$cmscontent->save();
				$request->session()->flash('message', 'FLOOD VENTS CREATED SUCCESSFULLY !!');
				return redirect()->route('admin.cmscontent.createfloodvent');
		}
		catch(Exception $e){

		}
	}
	public function removeresidentl(Request $request){
		Cmscontent::find($request->id)->delete();
		$request->session()->flash('message', 'RESIDENTIAL DELETED SUCCESSFULLY !!');
		return redirect()->route('admin.cmscontent.createresidentl');
	}
	public function removecomrcl(Request $request){
		Cmscontent::find($request->id)->delete();
		$request->session()->flash('message', 'COMMERCIAL DELETED SUCCESSFULLY !!');
		return redirect()->route('admin.cmscontent.createcomrcl');
	}

	
	public function createresidentl(Request $request)
    {
		$permission = Permission::permission('customer');
		$data=array();
       if($permission->add==1) {
			$data['cmscontentdiv3']=Cmscontent::where(['div_id'=>'3']);
			if($data['cmscontentdiv3']->count()>0){
				$data['editcontentdiv']=$data['cmscontentdiv3']->get();
			}
    		return view('admin.services.createresidentl',$data);
    	} else {
            return view('layouts.errors.403');
        }
	}
	public function storeresidentl(Request $request)
    {
		$permission = Permission::permission('customer');
		$data=array();
       if($permission->add==1) {
				$title = $request->title;
				$unitmainid = $request->mainid;
				for ($i=0; $i < count($title); $i++) { 
						if($unitmainid[$i]!=''){
							$getCnt=Cmscontent::where('id',$unitmainid[$i])->count();
							if($getCnt>0){
								$cmscontent = Cmscontent::find($unitmainid[$i]);
							}
							else{
								$cmscontent = New Cmscontent;
							}
						}
						else{
							$cmscontent = New Cmscontent;
						}
						$url = env('APP_URL');
						if ($request->has('image')) {
							if(!empty( $request->image[$i])){
								$file    = $request->image[$i];
								$time    = md5(time());
								$profile = $file->getClientOriginalExtension();
								$name = $file->getClientOriginalName();
								$path    = "storage/services";
								$file->move(public_path(env('IMAGE_URL').$path), $name);
								$cmscontent->image = $url."/storage/services" . "/" .$name;
							  }
							}
							$cmscontent->title = $request->title[$i];
							$cmscontent->page_id = $request->page_id;
							$cmscontent->div_id = $request->div_id;
							$cmscontent->html = $request->description[$i];
							
							$cmscontent->save();
				}
				
			
				//return view('admin.services.createresidentl',$data);
				$request->session()->flash('message', 'RESIDENTIAL CREATED SUCCESSFULLY !!');
				return redirect()->route('admin.cmscontent.createresidentl');
    	} else {
            return view('layouts.errors.403');
        }
	}
	public function createwhysmart(Request $request)
    {
		$permission = Permission::permission('customer');
		$data=array();
       if($permission->add==1) {

			$data['cmscontentdiv1']=Cmscontent::where(['div_id'=>'1']);
			$data['cmscontentdiv2']=Cmscontent::where(['div_id'=>'2']);
			$data['cmscontentdiv3']=Cmscontent::where(['div_id'=>'3']);
		
    			return view('admin.services.createwhysmart',$data);
    	} else {
            return view('layouts.errors.403');
        }
	}
	
	public function createcomrcl(Request $request)
    {
		$permission = Permission::permission('customer');
		$data=array();
       if($permission->add==1) {

			$data['cmscontentdiv3']=Cmscontent::where(['div_id'=>'4']);
			if($data['cmscontentdiv3']->count()>0){
				$data['editcontentdiv']=$data['cmscontentdiv3']->get();
			}
		
    			return view('admin.services.createcomrcl',$data);
    	} else {
            return view('layouts.errors.403');
        }
	}
	public function storecomrcl(Request $request){
		$permission = Permission::permission('customer');
		$data=array();
       if($permission->add==1) {
				$title = $request->title;
				$unitmainid = $request->mainid;
				for ($i=0; $i < count($title); $i++) { 
						if($unitmainid[$i]!=''){
							$getCnt=Cmscontent::where('id',$unitmainid[$i])->count();
							if($getCnt>0){
								$cmscontent = Cmscontent::find($unitmainid[$i]);
							}
							else{
								$cmscontent = New Cmscontent;
							}
						}
						else{
							$cmscontent = New Cmscontent;
						}
						$url = env('APP_URL');
						if ($request->has('image')) {
							if(!empty( $request->image[$i])){
								$file    = $request->image[$i];
								$time    = md5(time());
								$profile = $file->getClientOriginalExtension();
								$name = $file->getClientOriginalName();
								$path    = "storage/services";
								$file->move(public_path(env('IMAGE_URL').$path), $name);
								$cmscontent->image = $url."/storage/services" . "/" .$name;
							  }
							}
							$cmscontent->title = $request->title[$i];
							$cmscontent->page_id = $request->page_id;
							$cmscontent->div_id = $request->div_id;
							$cmscontent->html = $request->description[$i];
							
							$cmscontent->save();
				}
				
	   }
				//return view('admin.services.createresidentl',$data);
				$request->session()->flash('message', 'RESIDENTIAL CREATED SUCCESSFULLY !!');
				return redirect()->route('admin.cmscontent.createcomrcl');
	}
	public function createsmartventprodcts(Request $request){
		$permission = Permission::permission('customer');
		$data=array();
       if($permission->add==1) {

			$data['cmscontentdiv3']=Cmscontent::where(['div_id'=>'5']);
			if($data['cmscontentdiv3']->count()>0){
				$data['editcontentdiv']=$data['cmscontentdiv3']->first()->html;
			}
			else{
				$data['editcontentdiv']="";
			}
    			return view('admin.services.createsmartvntproduct',$data);
    	} else {
            return view('layouts.errors.403');
        }
	}
	public function storesmartventprodcts(Request $request){
		try{
			$getCnt=Cmscontent::where('div_id','5')->count();
			if($getCnt>0){
				$cmscontent=Cmscontent::where('div_id','5')->first();
			}
			else{
				$cmscontent=new Cmscontent;
			}
			//$cmscontent->

			$cmscontent->page_id = $request->page_id;
    	    $cmscontent->div_id = $request->div_id;
    	    $cmscontent->html = $request->description;
    	    $cmscontent->save();
    	    $request->session()->flash('message', 'WHY SMART VENT CREATED SUCCESSFULLY !!');
            return redirect()->route('admin.cmscontent.createsmartventprodcts');
		}
		catch(Exeception $e){

		}
	}
	public function createsmartvent(Request $request)
    {
		$permission = Permission::permission('customer');
		$data=array();
       if($permission->add==1) {

		if(Cmscontent::where(['div_id'=>2])->count()>0){
			$cmscontent= Cmscontent::where(['div_id'=>2])->first();
			$data['title']=$cmscontent->title;
			$data['description']=$cmscontent->html;
		}
		else{
			$cmscontent = new Cmscontent;
			$data['title']="";
			$data['description']="";
		}
		
    			return view('admin.services.createsmartvent',$data);
    	} else {
            return view('layouts.errors.403');
        }
	}
	public function storesmartvent(Request $request){
		try{
			if(Cmscontent::where(['div_id'=>2])->count()>0){
				$cmscontent= Cmscontent::where(['div_id'=>2])->first();
			
			}
			else{
				$cmscontent = new Cmscontent;
	
			}
		//	$cmscontent = new Cmscontent;
    	    $cmscontent->page_id = $request->page_id;
    	    $cmscontent->div_id = $request->div_id;
    	    $cmscontent->title = $request->title;
    	    $cmscontent->html = $request->description;
    	    $cmscontent->save();
    	    $request->session()->flash('message', 'WHY SMART VENT CREATED SUCCESSFULLY !!');
            return redirect()->route('admin.cmscontent.createsmartvent');
		}
		catch(Exception $e){

		}
	}
	public function createservices(Request $request)
    {
		$permission = Permission::permission('customer');
		$data=array();
       if($permission->add==1) {
		$data['cmscontentdiv3']=Cmscontent::where(['div_id'=>'6']);
			if($data['cmscontentdiv3']->count()>0){
				$data['editcontentdiv']=$data['cmscontentdiv3']->get();
			}
    		return view('admin.services.createservices',$data);
    	} else {
            return view('layouts.errors.403');
        }
	}
	public function storeservices(Request $request){
		try{
			$permission = Permission::permission('customer');
			$data=array();
		   if($permission->add==1) {
					$title = $request->title;
					$unitmainid = $request->mainid;
					for ($i=0; $i < count($title); $i++) { 
							if($unitmainid[$i]!=''){
								$getCnt=Cmscontent::where('id',$unitmainid[$i])->count();
								if($getCnt>0){
									$cmscontent = Cmscontent::find($unitmainid[$i]);
								}
								else{
									$cmscontent = New Cmscontent;
								}
							}
							else{
								$cmscontent = New Cmscontent;
							}
							$url = env('APP_URL');
							 if ($request->has('image')) {
								if(!empty( $request->image[$i])){
									$file    = $request->image[$i];
									$time    = md5(time());
									$profile = $file->getClientOriginalExtension();
									$name = $file->getClientOriginalName();
									$path    = "storage/services";
									$file->move(public_path(env('IMAGE_URL').$path), $file->getClientOriginalName());
									$cmscontent->image = $url."/storage/services" . "/" .$name;
								  }
								}
								$cmscontent->title = $request->title[$i];
								$cmscontent->page_id = $request->page_id;
								$cmscontent->div_id = $request->div_id;
								//$cmscontent->save();
                      
					}
                  
					
		   }
					//return view('admin.services.createresidentl',$data);
					$request->session()->flash('message', 'RESIDENTIAL CREATED SUCCESSFULLY !!');
					return redirect()->route('admin.cmscontent.createservices');
		}	
		catch(Exception $e){

		}
	}
	public function removeservice(Request $request){
		Cmscontent::find($request->id)->delete();
		$request->session()->flash('message', 'COMMERCIAL DELETED SUCCESSFULLY !!');
		return redirect()->route('admin.cmscontent.createservices');
	}
	public function createlokngsmrtvnt(Request $request)
    {
		$permission = Permission::permission('customer');
		$data=array();
       if($permission->add==1) {

			$data['cmscontentdiv1']=Cmscontent::where(['div_id'=>'1']);
			$data['cmscontentdiv2']=Cmscontent::where(['div_id'=>'2']);
			$data['cmscontentdiv3']=Cmscontent::where(['div_id'=>'3']);
		
    			return view('admin.services.createlokngsmrtvnt',$data);
    	} else {
            return view('layouts.errors.403');
        }
	}
	public function storelokngsmrtvnt(Request $request)
    {
		$permission = Permission::permission('customer');
		$data=array();
       if($permission->add==1) {

			//$data['cmscontentdiv1']=Cmscontent::where(['div_id'=>'1']);
		//	$data['cmscontentdiv2']=Cmscontent::where(['div_id'=>'2']);
			//$data['cmscontentdiv3']=Cmscontent::where(['div_id'=>'3']);
		
    		//	return view('admin.services.createlokngsmrtvnt',$data);
    	} else {
            return view('layouts.errors.403');
        }
	}
	public function createlookingforsmrtvnt(Request $request){
		$permission = Permission::permission('customer');
		$data=array();
       if($permission->add==1) {

			$data['cmscontentdiv3']=Cmscontent::where(['div_id'=>'7']);
			if($data['cmscontentdiv3']->count()>0){
				$data['editcontentdiv']=$data['cmscontentdiv3']->first()->html;
			}
			else{
				$data['editcontentdiv']="";
			}
    			return view('admin.services.createlookingforsmrtvnt',$data);
    	} else {
            return view('layouts.errors.403');
        }
	}
	public function storelookingforsmrtvnt(Request $request){
		try{
			$getCnt=Cmscontent::where('div_id','7')->count();
			if($getCnt>0){
				$cmscontent=Cmscontent::where('div_id','7')->first();
			}
			else{
				$cmscontent=new Cmscontent;
			}
			
			$cmscontent->page_id = $request->page_id;
    	    $cmscontent->div_id = $request->div_id;
    	    $cmscontent->html = $request->description;
    	    $cmscontent->save();
    	    $request->session()->flash('message', 'WHY SMART VENT CREATED SUCCESSFULLY !!');
            return redirect()->route('admin.cmscontent.createlookingforsmrtvnt');
		}
		catch(Exeception $e){

		}
	}
    public function store(Request $request){

    	if($request->div_id == 1)
    	{
    		//echo '<pre>';print_r($request->all());exit;
    		$cmscontent = new Cmscontent;
    	    $cmscontent->page_id = $request->page_id;
    	    $cmscontent->div_id = $request->div_id;
    	    $cmscontent->html = $request->description;
    	    $cmscontent->save();
    	    $request->session()->flash('message', 'FLOOD VENTS CREATED SUCCESSFULLY !!');
            return redirect()->route('admin.cmscontent.create');
    	}if($request->div_id == 2)
    	{
           //echo '<pre>';print_r($request->all());exit;
            $cmscontent = new Cmscontent;
    	    $cmscontent->page_id = $request->page_id;
    	    $cmscontent->div_id = $request->div_id;
    	    $cmscontent->title = $request->title;
    	    $cmscontent->html = $request->description;
    	    $cmscontent->save();
    	    $request->session()->flash('message', 'WHY SMART VENT CREATED SUCCESSFULLY !!');
            return redirect()->route('admin.cmscontent.create');
    	}if($request->div_id == 3)
    	{
		//    echo '<pre>';print_r($request->all());
		//    exit;
		//     $cmscontent = new Cmscontent;
			$title = $request->title;
			foreach($title as $key=>$value){
				$cmscontent = new Cmscontent;
				$url = env('APP_URL');
				if ($request->has('image')) {
				$file    = $request->image[$key];
				if(!empty($file)){
					$time    = md5(time());
					$profile = $file->getClientOriginalExtension();
					$name = $file->getClientOriginalName();
					$path    = "storage/services";
					$file->move(public_path(env('IMAGE_URL').$path), $name);
					$cmscontent->title = $value;
					$cmscontent->page_id = $request->page_id;
					$cmscontent->div_id = $request->div_id;
					$cmscontent->html = $request->description[$key];
					$cmscontent->image = $url."/storage/services" . "/" .$name;
					$cmscontent->save();
				}
				
			   }
			}
			///exit;
    	  /*  $cmscontent->page_id = $request->page_id;
    	    $cmscontent->div_id = $request->div_id;
    	    $cmscontent->title = $request->title;
    	    $cmscontent->html = $request->description;
    	    $url = env('APP_URL');
            if ($request->has('image')) {
            $file    = $request->image;
            $time    = md5(time());
            $profile = $file->getClientOriginalExtension();
            $name = $file->getClientOriginalName();
            $path    = "storage/services";
            $file->move(public_path(env('IMAGE_URL').$path), $name);
            
            $cmscontent->image = $url."/storage/services" . "/" .$name;
           }
    	   $cmscontent->save();
		   $request->session()->flash('message', 'RESIDENTIAL CREATED SUCCESSFULLY !!');*/
		   $request->session()->flash('message', 'RESIDENTIAL CREATED SUCCESSFULLY !!');
           return redirect()->route('admin.cmscontent.create');
    	}if($request->div_id == 4)
    	{
           //echo '<pre>';print_r($request->all());
            $cmscontent = new Cmscontent;
    	    $cmscontent->page_id = $request->page_id;
    	    $cmscontent->div_id = $request->div_id;
    	    $cmscontent->title = $request->title;
    	    $cmscontent->html = $request->description;
    	    $url = env('APP_URL');
            if ($request->has('image')) {
            $file    = $request->image;
            $time    = md5(time());
            $profile = $file->getClientOriginalExtension();
            $name = $file->getClientOriginalName();
            $path    = "storage/services";
            $file->move(public_path(env('IMAGE_URL').$path), $name);
            
            $cmscontent->image = $url."/storage/services" . "/" .$name;
           }
    	   $cmscontent->save();
    	   $request->session()->flash('message', 'COMMERCIAL CREATED SUCCESSFULLY !!');
           return redirect()->route('admin.cmscontent.create');
    	}
    	if($request->div_id == 5)
    	{
           //echo '<pre>';print_r($request->all());
            $cmscontent = new Cmscontent;
    	    $cmscontent->page_id = $request->page_id;
    	    $cmscontent->div_id = $request->div_id;
    	    $cmscontent->html = $request->description;
    	    $cmscontent->save();
    	    $request->session()->flash('message', 'SMART VENT PRODUCTS CREATED SUCCESSFULLY !!');
            return redirect()->route('admin.cmscontent.create');
    	}
    	if($request->div_id == 6)
    	{
           //echo '<pre>';print_r($request->all());
            $cmscontent = new Cmscontent;
    	    $cmscontent->page_id = $request->page_id;
    	    $cmscontent->div_id = $request->div_id;
    	    $cmscontent->title = $request->title;
    	    $url = env('APP_URL');
            if ($request->has('image')) {
            $file    = $request->image;
            $time    = md5(time());
            $profile = $file->getClientOriginalExtension();
            $name = $file->getClientOriginalName();
            $path    = "storage/services";
            $file->move(public_path(env('IMAGE_URL').$path), $name);
            
            $cmscontent->image = $url."/storage/services" . "/" .$name;
             }
    	    $cmscontent->save();
    	    $request->session()->flash('message', 'SERVICES CREATED SUCCESSFULLY !!');
            return redirect()->route('admin.cmscontent.create');
    	}if($request->div_id == 7)
    	{
           //echo '<pre>';print_r($request->all());
            $cmscontent = new Cmscontent;
    	    $cmscontent->page_id = $request->page_id;
    	    $cmscontent->div_id = $request->div_id;
    	    $cmscontent->title = $request->title;
    	    $cmscontent->save();
    	    $request->session()->flash('message', 'LOOKING FOR SMART VENTS CREATED SUCCESSFULLY !!');
            return redirect()->route('admin.cmscontent.create');
    	}
    	
    }
}
