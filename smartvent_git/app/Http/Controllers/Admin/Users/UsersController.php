<?php

namespace App\Http\Controllers\Admin\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\User;
use App\Helper\Permission;
use App\Shop\Roles\Role;
use App\Shop\RoleUsers\RoleUser;
use Hash;
use Validator;
use Auth;

class UsersController extends Controller
{
    public function index(Request $request){
    	try {
    		$permission = Permission::permission('users');
	        if($permission->view==1) {
	            $list = User::where('id','!=',Auth::guard('employee')->user()->id)->get();
	            return view('admin.user-role.list', [
	                'employees' => $list,
	                'permission' => $permission
	            ]);
	        } else {
	            return view('layouts.errors.403');
	        }
    	} catch (Exception $e) {
    		Log::error($e);
    	}
    }
    public function create()
    {
    	try {
	        $permission = Permission::permission('users');
	        if($permission->add==1) {
	            $roles = Role::orderBy('name', 'ASC')->get();
	            return view('admin.user-role.create', compact('roles'));
	        } else {
	            return view('layouts.errors.403');
	        }
    	} catch (Exception $e) {
    		Log::error($e);	
    	}
    }
    public function store(Request $request)
    {
    	try {
    		$rules = [
                'name' => 'required|string|max:255',
                'mobile' => 'required|numeric|digits_between:8,15',
                'email' => 'required',
                'status' => 'required',
                // 'password' => 'required',
                'role' => 'required'
                // 'logo' =>'required',
            ];
            $customeMessage = [
                'name.required' => 'Please enter name.',
                'mobile.required' => 'Please enter contact number',
                'mobile.numeric' => 'Please enter number',
                'mobile.unique' => 'Mobile number already exist.',
                'mobile.digits_between' => 'Please enter only digits and maximum 15 digits.',
                'mobile.unique' => 'Contact number is already exists.',
                'email' => 'Email is required',
                'password' => 'Password is required',
                'status' => 'Status is required',
                'password' => 'Password is required',
                'role' => 'Role is required',
                // 'logo' => 'Address is required',
            ];
            if (isset($request->id)) {
                $users = User::find($request->id);
                $oldMobile = $users->mobile;
                $newMobile = $request->mobile;
                if ($oldMobile!=$newMobile) {
                    $rules['mobile'] = 'required|unique:users,mobile';
                }
                if ($users->email!=$request->email) {
                    $rules['email'] = 'required|unique:users,email';
                }
            } else{
            	$rules['password'] = 'required';
                $rules['mobile'] = 'required|unique:users,mobile';
                $rules['email'] = 'required|unique:users,email';
            }
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if( $validator->fails() ) {
                return back()->withErrors($validator->errors())->withInput();
            } else{
            	// dd($request->all());
	    		$permission = Permission::permission('users');
	        	if($permission->add==1) {
	        		if ($request->id!="") {
	        			$user =  User::find($request->id);
	        		}else{
	        			$user =  new User();
	        		}
	        		$user->name = $request->name;
	        		$user->name_hi = $request->name_hi;
	        		$user->name_gu = $request->name_gu;
	        		$user->mobile = $request->mobile;
	        		$user->email = $request->email;
	        		$user->password = Hash::make($request->password);
	        		$user->status = $request->status;
	        		if ($user->save()) {
	        			if ($request->hasFile('logo')) {
		                    $url = env('APP_URL');
		                    $file    = $request->logo;
		                    $time    = md5(time());
		                    $profile = $file->getClientOriginalExtension();
		                    $name = $file->getClientOriginalName();
		                    $path    = "storage/logo";
		                    $file->move(public_path(env('IMAGE_URL').$path), $time.'.'.$profile);
		                    $profile_update          = User::find($user->id);
		                    $profile_update->logo = $url."/".$path.'/'.$time.'.'.$profile;
		                    $profile_update->save();
		                }
		                if ($request->id!="") {
		                	$user_role = RoleUser::where('user_id',$user->id)->first();
		                }else{
							$user_role = new RoleUser();
		                }
		                $user_role->role_id = $request->role;
		                $user_role->user_id = $user->id; 
		                $user_role->user_type = 'App\Shop\Employees\Employee'; 
		                $user_role->save();     
		                // dd($user_role);
	        		}
	        		if ($request->id!="") {
	        			return redirect()->route('admin.users.index')->with('message', 'User Update Successfully');
	        		}else{
	        			return redirect()->route('admin.users.index')->with('message', 'User Added Successfully');
	        		}
	        	} else {
	            	return view('layouts.errors.403');
	        	}
	        }
    	} catch (Exception $e) {
    		Log::error($e);
    	}
    }
    public function edit($id)
    {
    	try {
    		$permission = Permission::permission('users');
	        if($permission->add==1) {
	            $roles = Role::orderBy('name', 'ASC')->get();
	            $selectedIds = RoleUser::where('user_id',$id)->first()->role_id;
	            $user = User::where('id',$id)->first();
	            return view('admin.user-role.edit', compact('roles','user','selectedIds'));
	        } else {
	            return view('layouts.errors.403');
	        }
    	} catch (Exception $e) {
    		Log::error($e);	
    	}
    }
    public function destroy($id)
    {
    	try {
    		$permission = Permission::permission('users');
	        if($permission->add==1) {
	            $roles = Role::orderBy('name', 'ASC')->get();
	            $user_role = RoleUser::where('user_id',$id)->first();
	            $user_role->delete();
	            $user = User::where('id',$id)->first();
	            $user->delete();
             	return redirect()->route('admin.users.index')->with('message', 'User Deleted Successfully');
	        } else {
	            return view('layouts.errors.403');
	        }
    	} catch (Exception $e) {
    		Log::error($e);	
    	}
    }
}
