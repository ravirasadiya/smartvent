<?php

namespace App\Http\Controllers\Admin\Customers;

use App\Shop\Customers\Customer;
use App\Shop\Customers\Repositories\CustomerRepository;
use App\Shop\Customers\Repositories\Interfaces\CustomerRepositoryInterface;
use App\Shop\Customers\Requests\CreateCustomerRequest;
use App\Shop\Customers\Requests\UpdateCustomerRequest;
use App\Shop\Customers\Transformations\CustomerTransformable;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\CustomersEmail;
use App\Helper\Permission;
use App\Shop\Addresses\Address;
use App\Shop\CountriesData\CountryData;

class CustomerController extends Controller
{
    use CustomerTransformable;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepo;

    /**
     * CustomerController constructor.
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->customerRepo = $customerRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permission = Permission::permission('customer');
        if($permission->view==1) {
            $list = $this->customerRepo->listCustomers('created_at', 'desc');

            if (request()->has('q')) {
                $list = $this->customerRepo->searchCustomer(request()->input('q'));
            }

            $customers = $list->map(function (Customer $customer) {
                return $this->transformCustomer($customer);
            })->all();


            return view('admin.customers.list', [
                'customers' => $this->customerRepo->paginateArrayResults($customers),
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission = Permission::permission('customer');
        if($permission->add==1) {
            $states = CountryData::select('STCode', 'DTName')->where('DTCode',000)->orderBy('DTName','ASC')->get();
            return view('admin.customers.create', ['states' =>$states]);
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCustomerRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCustomerRequest $request)
    {
        $permission = Permission::permission('customer');
        if($permission->add==1) {
            $allrequest = $request->except('_token', '_method');
            $allrequest['state_id'] = $request->states;
            $allrequest['city_id'] = $request->cities;
            $allrequest['tehsil_id'] = $request->tehsils;
            $allrequest['village_id'] = $request->villages;
            $allrequest['language'] = 'en';
            $customer=$this->customerRepo->createCustomer($allrequest);
            // $customer->state_id = $request->states;
            // $customer->city_id = $request->cities;
            // $customer->tehsil_id = $request->tehsils;
            // $customer->village_id = $request->villages;
            // $customer->language = 'en';
            // // $customer->save();
            // // $customer->alias = $request->name;
            // $customer->address = $request->address;
            // $customer->city = $request->cities;
            // $customer->state_code = $request->states;
            // $customer->tehsil_id = $request->tehsils;
            // $customer->village_id = $request->villages;
            // $customer->phone = $request->mobile;
            // $customer->country_id = 99;
            // $customer->customer_id = $customer->id;
            // $customer->save();
            $request->session()->flash('message', 'Customer create successfully!');
            return redirect()->route('admin.customers.index');
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $customer = $this->customerRepo->findCustomerById($id);
        $addresses = Address::where('customer_id', $customer->id)->orderBy('id', 'DESC')->first();
        $state = CountryData::select('DTName')->where('STCode', 'like', '%' .$addresses->state_code)->where('DTCode',000)->first();
	    $city = CountryData::select('DTName')->where('DTCode', 'like', '%' .$addresses->city)->first();
	    $tehsil = CountryData::select('SDTName')->where('SDTCode','like', '%' .$addresses->tehsil_id)->first();
	    $village = CountryData::select('Name')->where('TVCode','like', '%' .$addresses->village_id)->first();
        $addresses->state_code = $state->DTName;
        $addresses->city = $city->DTName;
        $addresses->tehsil_id = $tehsil->SDTName;
        $addresses->village_id = $village->Name;
        return view('admin.customers.show', [
            'customer' => $customer,
            'addresses' => $addresses
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = Permission::permission('customer');
        if($permission->edit==1) {
            $customer = $this->customerRepo->findCustomerById($id);
            // $addresses = Address::where('customer_id', $id)->orderBy('id', 'DESC')->first();
            $state = CountryData::select('STCode', 'DTName')->where('DTCode',000)->orderBy('DTName', 'ASC')->get();
    	    $city = CountryData::select('DTCode', 'DTName')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' . $customer->state_id)->orderBy('DTName', 'ASC')->get();
    	    $tehsil = countrydata::select('SDTCode', 'SDTName')->where('DTCode', 'like', '%' .$customer->city_id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->orderBy('SDTName', 'ASC')->get();
            $village = countrydata::select('TVCode', 'Name')->where('SDTCode', 'like', '%' .$customer->tehsil_id)->where('TVCode', '!=',000000)->orderBy('Name', 'ASC')->get();
            // dd('e');
            return view('admin.customers.edit', [
                'customer' => $customer,
                // 'address' => $addresses,
                'states' => $state,
                'cities' => $city,
                'tehsils' => $tehsil,
                'villages' => $village
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCustomerRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCustomerRequest $request, $id)
    {
        $permission = Permission::permission('customer');
        if($permission->edit==1) {
            // $allrequest = $request->except('_token', '_method');
            $customer = $this->customerRepo->findCustomerById($id);

            if ($request->input('status') != $customer->status) {
                $data = $customer;
                $allrequest['status'] = $request->input('status');
                Mail::to($request->input('email'))->send(new CustomersEmail($data));
            }

            $update = new CustomerRepository($customer);
            $allrequest = $request->except('_method', '_token', 'password');

            if ($request->has('password')) {
                $allrequest['password'] = bcrypt($request->input('password'));
            }
            $allrequest['state_id'] = $request->states;
            $allrequest['city_id'] = $request->cities;
            $allrequest['tehsil_id'] = $request->tehsils;
            $allrequest['village_id'] = $request->villages;
            $res = $update->updateCustomer($allrequest);

            // dd($res);
            // $this->customerRepo->updateCustomer($allrequest);
            // $customer->save();
            
            /*$address = new Address();
            $address->alias = $request->name;
            $address->address_1 = $request->address;
            $address->zip = $request->zip;
            $address->city = $request->cities;
            $address->state_code = $request->states;
            $address->tehsil_id = $request->tehsils;
            $address->village_id = $request->villages;
            $address->phone = $request->mobile;
            $address->country_id = 99;
            $address->customer_id = $customer->id;
            $address->save();*/
            
            $request->session()->flash('message', 'Updated customer successfully');
            return redirect()->route('admin.customers.index');
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $permission = Permission::permission('customer');
        if($permission->delete==1) {
            $customer = $this->customerRepo->findCustomerById($id);

            $customerRepo = new CustomerRepository($customer);
            $customerRepo->deleteCustomer();

            return redirect()->route('admin.customers.index')->with('message', 'Customer delete successfully');
        } else {
            return view('layouts.errors.403');
        }
    }
}
